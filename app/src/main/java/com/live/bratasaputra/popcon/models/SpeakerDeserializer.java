package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class SpeakerDeserializer implements JsonDeserializer<Speaker> {
    public static Speaker DeserializeFrom(JsonObject jsonObject) {
        Speaker speaker = new Speaker();
        if (jsonObject.has("id")) {
            speaker.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("slug")) {
            speaker.slug = jsonObject.get("slug").getAsString();
        } else if (jsonObject.has("post_name")) {
            speaker.slug = jsonObject.get("post_name").getAsString();
        }
        if (jsonObject.has("link")) {
            speaker.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("links")) {
            speaker.links = jsonObject.get("links").getAsString();
        }
        if (jsonObject.has("featured_media_url")) {
            speaker.featuredMediaUrl = jsonObject.get("featured_media_url").getAsString();
        }
        if (jsonObject.has("jabatan")) {
            speaker.jabatan = jsonObject.get("jabatan").getAsString();
        }
        if (jsonObject.has("organisasi")) {
            speaker.organisasi = jsonObject.get("organisasi").getAsString();
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonPrimitive()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                if (titleObject.has("rendered")) {
                    speaker.title = titleObject.get("rendered").getAsString();
                }
            }
        } else if (jsonObject.has("post_title")) {
            speaker.title = jsonObject.get("post_title").getAsString();
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                if (contentObject.has("rendered")) {
                    speaker.content = contentObject.get("rendered").getAsString();
                }
            }
        } else if (jsonObject.has("post_content")) {
            speaker.content = jsonObject.get("post_content").getAsString();
        }
        if (jsonObject.has("activities")) {
            JsonElement activityElement = jsonObject.get("activities");
            if (activityElement.isJsonArray()) {
                speaker.activityList = new ArrayList<>();
                JsonArray activityArray = activityElement.getAsJsonArray();
                for (JsonElement element :
                        activityArray) {
                    JsonObject activityObject = element.getAsJsonObject();
                }
            }
        }
        if (jsonObject.has("exhibitor")) {
            JsonElement exhibitorElement = jsonObject.get("exhibitor");
            if (exhibitorElement.isJsonArray()) {
                speaker.exhibitorList = new ArrayList<>();
                JsonArray exhibitorArray = exhibitorElement.getAsJsonArray();
                for (JsonElement element :
                        exhibitorArray) {
                    JsonObject exhibitorObject = element.getAsJsonObject();
                }
            }
        }
        if (jsonObject.has("company")) {
            JsonElement companyElement = jsonObject.get("company");
            if (companyElement.isJsonArray()) {
                speaker.companyList = new ArrayList<>();
                JsonArray companyArray = companyElement.getAsJsonArray();
                for (JsonElement element :
                        companyArray) {
                    JsonObject companyObject = element.getAsJsonObject();
                }
            }
        }
        return speaker;
    }

    @Override
    public Speaker deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return SpeakerDeserializer.DeserializeFrom(jsonObject);
    }
}
