package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Promotion;

/**
 * Created by bratasaputra on 8/1/17.
 */

public class OpenPromotionEvent {
    public Promotion promotion;

    public OpenPromotionEvent(Promotion promotion) {
        this.promotion = promotion;
    }
}
