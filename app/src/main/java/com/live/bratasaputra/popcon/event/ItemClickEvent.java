package com.live.bratasaputra.popcon.event;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ItemClickEvent<T> extends BaseEvent<T> {
    public ItemClickEvent(T item) {
        super(item);
        setItem(item);
    }
}
