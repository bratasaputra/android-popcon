package com.live.bratasaputra.popcon.models;

import java.util.List;

public class ScheduleGroupedByLocation {
    public List<ScheduleGroupedByDates> mainStage;
    public List<ScheduleGroupedByDates> parade;
    public List<ScheduleGroupedByDates> workshop;
    public List<ScheduleGroupedByDates> masterclass;
    public List<ScheduleGroupedByDates> popConnect;
    public List<ScheduleGroupedByDates> photoSession;
    public List<ScheduleGroupedByDates> signingSession;
}
