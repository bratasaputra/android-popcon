package com.live.bratasaputra.popcon;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.live.bratasaputra.popcon.adapter.ExhibitorAdapter;
import com.live.bratasaputra.popcon.models.Exhibitor;
import com.live.bratasaputra.popcon.models.ExhibitorCategory;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExhibitorListFragment extends Fragment {
    protected RecyclerView mExhibitorList;
    protected SwipeRefreshLayout mRefreshLayout;
    protected ProgressBar mProgress;
    protected RecyclerView.LayoutManager mLayout;
    protected ExhibitorAdapter mAdapter;

    protected boolean isDownloading;
    protected int currentPage;
    protected int limit;
    protected List<Exhibitor> exhibitorTempList;
    protected Realm realm;

    public ExhibitorListFragment() {
        // Required empty public constructor
    }

    public static ExhibitorListFragment Create() {
        ExhibitorListFragment fragment = new ExhibitorListFragment();
        return fragment;
    }

    private void refreshContent() {
        if (mAdapter.getItemCount() == 0) {
            showLoading();
        }
        currentPage = currentPage + 1;
        isDownloading = true;
        PopCon.getDefaultApiClient().GetExhibitors(currentPage, limit).enqueue(new Callback<List<Exhibitor>>() {
            @Override
            public void onResponse(Call<List<Exhibitor>> call, Response<List<Exhibitor>> response) {
                switch (response.code()) {
                    case 200:
                        List<Exhibitor> result = response.body();
                        exhibitorTempList.addAll(result);
                        if (result.size() == limit) {
                            refreshContent();
                        } else {
                            realm.beginTransaction();
                            realm.delete(Exhibitor.class);
                            realm.copyToRealmOrUpdate(exhibitorTempList);
                            realm.commitTransaction();
                            reloadData();
                        }
                        break;
                    default:
                        reloadData();
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Exhibitor>> call, Throwable t) {
                reloadData();
            }
        });
    }

    private void loadCategory() {
        if (mAdapter.getItemCount() == 0) {
            showLoading();
        }
        isDownloading = true;
        currentPage = 0;
        PopCon.getDefaultApiClient().GetExhibitorCategory(limit).enqueue(new Callback<List<ExhibitorCategory>>() {
            @Override
            public void onResponse(Call<List<ExhibitorCategory>> call, Response<List<ExhibitorCategory>> response) {
                switch (response.code()) {
                    case 200:
                        List<ExhibitorCategory> result = response.body();
                        realm.beginTransaction();
                        realm.delete(ExhibitorCategory.class);
                        realm.copyToRealmOrUpdate(result);
                        realm.commitTransaction();
                        refreshContent();
                        break;
                    default:
                        reloadData();
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<ExhibitorCategory>> call, Throwable t) {
                reloadData();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exhibitor_list, container, false);
        mExhibitorList = (RecyclerView) view.findViewById(R.id.exhibitor_list);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        mProgress = (ProgressBar) view.findViewById(R.id.exhibitor_load_progress);
        mAdapter = new ExhibitorAdapter(new ArrayList<Exhibitor>(), getContext());
        mExhibitorList.setAdapter(mAdapter);
        mLayout = new GridLayoutManager(getContext(), 2);
        mExhibitorList.setLayoutManager(mLayout);

        isDownloading = false;
        currentPage = 0;
        limit = 100;
        exhibitorTempList = new ArrayList<>();

        realm = Realm.getDefaultInstance();

//        setHasOptionsMenu(true);
        hideLoading();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            if (isDownloading) {
                Snackbar.make(mExhibitorList, "Please wait...", Snackbar.LENGTH_SHORT).show();
            } else {
                isDownloading = false;
                currentPage = 0;
                mAdapter.setData(new ArrayList<Exhibitor>());
                exhibitorTempList.clear();
                loadCategory();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        reloadData();

        if (mAdapter.getItemCount() == 0) {
            loadCategory();
        }
    }

    private void reloadData() {
        isDownloading = false;
        hideLoading();
        RealmResults<Exhibitor> list = realm.where(Exhibitor.class).findAllSorted("title");
        mAdapter.setData(list);
    }

    private void showLoading() {
        mProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        mProgress.setVisibility(View.GONE);
    }
}
