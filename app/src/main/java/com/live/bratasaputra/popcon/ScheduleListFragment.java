package com.live.bratasaputra.popcon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.live.bratasaputra.popcon.adapter.ScheduleAdapter;
import com.live.bratasaputra.popcon.event.OpenScheduleEvent;
import com.live.bratasaputra.popcon.event.SCHEDULENAME;
import com.live.bratasaputra.popcon.models.Schedule;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByDates;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByLocation;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleListFragment extends Fragment {
    protected RecyclerView mRecyclerView = null;
    protected RecyclerView.LayoutManager mLayoutManager = null;
    protected ScheduleAdapter mAdapter = null;
    protected ProgressBar mProgressBar = null;
    SCHEDULENAME schedulename;

    public ScheduleListFragment() {
        // Required empty public constructor
    }

    public static ScheduleListFragment Create() {
        ScheduleListFragment fragment = new ScheduleListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedule_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.schedule_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ScheduleAdapter(new ArrayList<ScheduleGroupedByDates>());
        mRecyclerView.setAdapter(mAdapter);
        mProgressBar = (ProgressBar) view.findViewById(R.id.schedule_load_progress);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        OpenScheduleEvent event = EventBus.getDefault().removeStickyEvent(OpenScheduleEvent.class);
        if(event != null) {
            schedulename = event.schedulename;
            Util.Log(schedulename.toString());
            PopCon.getDefaultApiClient().GetSchedule().enqueue(new Callback<ScheduleGroupedByLocation>() {
                @Override
                public void onResponse(Call<ScheduleGroupedByLocation> call, Response<ScheduleGroupedByLocation> response) {
                    mProgressBar.setVisibility(View.GONE);
                    switch (response.code()) {
                        case 200:
                            ScheduleGroupedByLocation x = response.body();
                            switch (schedulename) {
                                case MAINSTAGE:
                                    mAdapter.setData(x.mainStage);
                                    break;
                                case POPCONNECT:
                                    mAdapter.setData(x.popConnect);
                                    break;
                                case WORKSHOP:
                                    mAdapter.setData(x.workshop);
                                    break;
                                case PARADE:
                                    mAdapter.setData(x.parade);
                                    break;
                                case MASTERCLASS:
                                    mAdapter.setData(x.masterclass);
                                    break;
                                case PHOTO_SESSION:
                                    mAdapter.setData(x.photoSession);
                                    break;
                                case SIGNING_SESSION:
                                    mAdapter.setData(x.signingSession);
                                    break;
                            }
                            break;
                    }
                }

                @Override
                public void onFailure(Call<ScheduleGroupedByLocation> call, Throwable t) {
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        } else {
            getActivity().finish();
        }
    }
}
