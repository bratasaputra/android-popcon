package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Exhibitor;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenExhibitorEvent extends OpenItemEvent<Exhibitor> {
    public OpenExhibitorEvent(Exhibitor item) {
        super(item);
        setItem(item);
    }
}
