package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class CompanyDeserializer implements JsonDeserializer<Company> {
    public static Company DeserializeFrom(JsonObject jsonObject) {
        Company company = new Company();
        if (jsonObject.has("id")) {
            company.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("slug")) {
            company.slug = jsonObject.get("slug").getAsString();
        } else if (jsonObject.has("post_name")) {
            company.slug = jsonObject.get("post_name").getAsString();
        }
        if (jsonObject.has("link")) {
            company.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("featured_media_url")) {
            company.featuredMediaUrl = jsonObject.get("featured_media_url").getAsString();
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonPrimitive() && !titleElement.isJsonNull()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                if (titleObject.has("rendered")) {
                    company.title = titleObject.get("rendered").getAsString();
                }
            }
        } else if (jsonObject.has("post_title")) {
            company.title = jsonObject.get("post_title").getAsString();
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive() && !contentElement.isJsonNull()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                if (contentObject.has("rendered")) {
                    company.content = contentObject.get("rendered").getAsString();
                }
            }
        }
        return company;
    }

    @Override
    public Company deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return CompanyDeserializer.DeserializeFrom(jsonObject);
    }
}
