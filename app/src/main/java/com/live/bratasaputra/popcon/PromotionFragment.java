package com.live.bratasaputra.popcon;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.live.bratasaputra.popcon.adapter.PromotionAdapter;
import com.live.bratasaputra.popcon.models.Promotion;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PromotionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PromotionFragment extends Fragment {
    RecyclerView mPromotionList;
    List<Promotion> promotions;
    PromotionAdapter mAdapters;
    RecyclerView.LayoutManager mLayout;
    ProgressBar mProgressBar;
    FirebaseAnalytics analytics;

    public PromotionFragment() {
        // Required empty public constructor
    }

    public static PromotionFragment newInstance() {
        PromotionFragment fragment = new PromotionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        analytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promotion, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mPromotionList = (RecyclerView) view.findViewById(R.id.promtion_list);
        mAdapters = new PromotionAdapter(new ArrayList<Promotion>(), getContext());
        mPromotionList.setAdapter(mAdapters);
        mLayout = new LinearLayoutManager(getContext());
        mPromotionList.setLayoutManager(mLayout);

//        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_menu, menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PopCon.getDefaultApiClient().GetPromotions(100).enqueue(new Callback<List<Promotion>>() {
            @Override
            public void onResponse(Call<List<Promotion>> call, Response<List<Promotion>> response) {
                mProgressBar.setVisibility(View.GONE);
                switch (response.code()) {
                    case 200:
                        List<Promotion> promotions = response.body();
                        mAdapters.setPromotions(promotions);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Promotion>> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Util.Log("Error");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
