package com.live.bratasaputra.popcon.models;

import java.util.List;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class Schedule {
    public Integer id;
    public String slug;
    public String link;
    public String title;
    public String content;
    public List<Activities> activityList;
    public List<Company> companyList;
    public List<Exhibitor> exhibitorList;
    public List<Speaker> speakerList;
    public String timeStart1;
    public String timeEnd1;
    public String timeStart2;
    public String timeEnd2;
    public String timeStart3;
    public String timeEnd3;
}

