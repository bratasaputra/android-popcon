package com.live.bratasaputra.popcon;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.event.OpenSpeakerEvent;
import com.live.bratasaputra.popcon.models.Speaker;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

public class SpeakerDetailActivity extends AppCompatActivity {
    protected Speaker mSpeaker;
    protected ImageView mImage;
    protected TextView mInfo;
    protected Toolbar mToolbar;
    protected WebView mDesc;
    protected Button mLink;

    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_detail);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImage = (ImageView) findViewById(R.id.speaker_image);
        mInfo = (TextView) findViewById(R.id.speaker_info);
        mDesc = (WebView) findViewById(R.id.speaker_desc);
        mDesc.setBackgroundColor(getResources().getColor(android.R.color.transparent));
//        mLink = (Button) findViewById(R.id.speaker_link);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        OpenSpeakerEvent event = EventBus.getDefault().removeStickyEvent(OpenSpeakerEvent.class);
        if (event != null) {
            mSpeaker = event.getItem();
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, mSpeaker.id);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mSpeaker.title);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "People");
            analytics = FirebaseAnalytics.getInstance(this);
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
//            mLink.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mSpeaker.links));
//                    startActivity(intent);
//                }
//            });
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle(mSpeaker.title);
        Glide.with(mImage.getContext())
                .load(mSpeaker.featuredMediaUrl)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(mImage);
        mInfo.setText(String.format(Locale.getDefault(), "%s, %s", mSpeaker.jabatan, mSpeaker.organisasi));
//        Document document = Jsoup.parse(mSpeaker.content);
//        if (document != null) {
//            mDesc.setText(document.text());
//        }
        mDesc.loadData(mSpeaker.content, "text/html", null);
    }
}
