package com.live.bratasaputra.popcon;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;


public class InterestFragment extends Fragment {
    Button activityButton;
    Button exhibitorButton;
    Button speakerButton;
    ImageView background;

    public InterestFragment() {
        // Required empty public constructor
    }

    public static InterestFragment newInstance() {
        InterestFragment fragment = new InterestFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_interest, container, false);
        activityButton = (Button)view.findViewById(R.id.activity_button);
        activityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showActivity = new Intent(getContext(), ActivityListActivity.class);
                startActivity(showActivity);
            }
        });
        exhibitorButton = view.findViewById(R.id.exhibitor_button);
        exhibitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showExhibitor = new Intent(getContext(), ExhibitorListActivity.class);
                startActivity(showExhibitor);
            }
        });
        speakerButton = view.findViewById(R.id.popstar_button);
        speakerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showPopstar = new Intent(getContext(), SpeakerListActivity.class);
                startActivity(showPopstar);
            }
        });
        background = view.findViewById(R.id.background);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Glide.with(this).load(R.drawable.poppo_red_dark).into(background);
    }
}
