package com.live.bratasaputra.popcon.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Speaker;

import java.util.List;

/**
 * Created by bratasaputra on 5/13/17.
 */

public class SpeakerAdapter extends RecyclerView.Adapter<SpeakerViewHolder> {
    private List<Speaker> mSpeakers;

    public SpeakerAdapter(@NonNull List<Speaker> speakers) {
        mSpeakers = speakers;
    }

    public List<Speaker> getSpeakers() {
        return mSpeakers;
    }

    public void setSpeakers(@NonNull List<Speaker> speakers) {
        mSpeakers = speakers;
        notifyDataSetChanged();
    }

    @Override
    public SpeakerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.speaker_card_view, parent, false);
        return new SpeakerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpeakerViewHolder holder, int position) {
        holder.bind(mSpeakers.get(position));
    }

    @Override
    public int getItemCount() {
        return mSpeakers.size();
    }
}
