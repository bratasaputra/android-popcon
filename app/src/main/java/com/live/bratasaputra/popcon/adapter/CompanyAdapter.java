package com.live.bratasaputra.popcon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 5/2/17.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyViewHolder> {
    protected List<Company> mCompany = new ArrayList<>();

    public CompanyAdapter(List<Company> companies) {
        if (companies != null) {
            this.mCompany = companies;
        }
    }

    public void setAdapter(List<Company> companies) {
        this.mCompany = companies;
        notifyDataSetChanged();
    }

    @Override
    public CompanyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_card_view, parent, false);
        return new CompanyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CompanyViewHolder holder, int position) {
        holder.bind(mCompany.get(position));
    }

    @Override
    public int getItemCount() {
        if (mCompany != null) {
            return mCompany.size();
        }
        return 0;
    }
}
