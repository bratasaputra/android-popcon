package com.live.bratasaputra.popcon.event;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class BaseEvent<T> {
    protected T mItem;

    public BaseEvent(T item) {
        this.mItem = item;
    }

    public T getItem() {
        return mItem;
    }

    public void setItem(T item) {
        this.mItem = item;
    }
}
