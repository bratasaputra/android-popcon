package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.media.tv.TvContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.ScheduleItemClickEvent;
import com.live.bratasaputra.popcon.models.Schedule;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByDates;
import com.live.bratasaputra.popcon.models.ScheduleItem;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by bratasaputra on 5/4/17.
 */

public class ScheduleViewHolder extends RecyclerView.ViewHolder {
    protected Context mContext;
    protected TextView mDate;
    ScheduleGroupedByDates scheduleGroupedByDates;
    RecyclerView list;
    ScheduleItemAdapter adapter;
    RecyclerView.LayoutManager mlayout;

    public ScheduleViewHolder(View itemView) {
        super(itemView);
        mDate = itemView.findViewById(R.id.schedule_date);
        list = itemView.findViewById(R.id.schedule_list);
        adapter = new ScheduleItemAdapter(new ArrayList<ScheduleItem>());
        list.setAdapter(adapter);
        mlayout = new LinearLayoutManager(itemView.getContext());
        list.setLayoutManager(mlayout);
    }

    public void bind(ScheduleGroupedByDates date) {
        scheduleGroupedByDates = date;
        mDate.setText(scheduleGroupedByDates.dateString);
        adapter.setData(scheduleGroupedByDates.scheduleItems);
    }
}
