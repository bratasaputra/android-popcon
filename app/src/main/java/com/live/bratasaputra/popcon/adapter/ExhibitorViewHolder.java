package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.PopCon;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.ExhibitorItemClickEvent;
import com.live.bratasaputra.popcon.models.Exhibitor;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ExhibitorViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    private ImageView mImage;
    private TextView mTitle;
    private TextView mBooth;
    private Exhibitor mExhibitor;
    private Context mContext;
    private ExhibitorAdapter.OnItemClickListener mListener = null;

    public ExhibitorViewHolder(View view, Context glideContext) {
        super(view);
        mContext = glideContext;
        mImage = (ImageView) view.findViewById(R.id.exhibitor_image);
        mTitle = (TextView) view.findViewById(R.id.exhibitor_title);
        mBooth = (TextView) view.findViewById(R.id.exhibitor_booth);
        view.setOnClickListener(this);
    }

    public void bind(Exhibitor item) {
        this.mExhibitor = item;
        this.mTitle.setText(Jsoup.parse(mExhibitor.title).text());
        String booth = mExhibitor.boothNumber;
        if (mExhibitor.boothNumber2 != null) {
            booth = String.format("%s - %s", booth, mExhibitor.boothNumber2);
        }
        mBooth.setText(booth);
        if (item.featuredMediaUrl != null) {
            Glide.with(mContext).load(item.featuredMediaUrl).placeholder(R.drawable.placeholder).into(mImage);
        } else {
            PopCon.getDefaultApiClient().GetExhibitors(mExhibitor.id).enqueue(new Callback<Exhibitor>() {
                @Override
                public void onResponse(Call<Exhibitor> call, Response<Exhibitor> response) {
                    switch (response.code()) {
                        case 200:
                            mExhibitor = response.body();
                            Glide.with(mContext).load(mExhibitor.featuredMediaUrl).placeholder(R.drawable.placeholder).into(mImage);
                            break;
                    }
                }

                @Override
                public void onFailure(Call<Exhibitor> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) {
            ExhibitorItemClickEvent event = new ExhibitorItemClickEvent(this.mExhibitor);
            EventBus.getDefault().post(event);
        } else {
            mListener.OnItemClick(this.mExhibitor);
        }
    }

    public void setItemClickListener(ExhibitorAdapter.OnItemClickListener listener) {
        mListener = listener;
    }
}
