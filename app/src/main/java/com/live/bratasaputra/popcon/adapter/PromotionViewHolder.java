package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.PromotionDetailActivity;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.OpenPromotionEvent;
import com.live.bratasaputra.popcon.models.Promotion;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by bratasaputra on 7/31/17.
 */

public class PromotionViewHolder extends RecyclerView.ViewHolder {
    AppCompatTextView mTitle;
    ImageView mImage;
    Context mContext;
    Promotion mPromotion;

    public PromotionViewHolder(View view, Context fragment) {
        super(view);
        mTitle = (AppCompatTextView) view.findViewById(R.id.promotion_title);
        mImage = (ImageView) view.findViewById(R.id.promotion_image);
        mContext = fragment;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenPromotionEvent(mPromotion));
                view.getContext().startActivity(new Intent(view.getContext(), PromotionDetailActivity.class));
            }
        });
    }

    public void bindViewHolder(Promotion promotion) {
        mPromotion = promotion;
        mTitle.setText(mPromotion.title);
        Glide.with(mContext).load(mPromotion.featuredMediaUrl).centerCrop().into(mImage);
    }
}
