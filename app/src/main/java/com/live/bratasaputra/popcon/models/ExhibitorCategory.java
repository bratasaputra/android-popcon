package com.live.bratasaputra.popcon.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bratasaputra on 6/12/17.
 */

public class ExhibitorCategory extends RealmObject {
    @PrimaryKey
    public int id;
    public String name;
    public String shortname;
    public String desc;
    public String slug;
    public String link;
}
