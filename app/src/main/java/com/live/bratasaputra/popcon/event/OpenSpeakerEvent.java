package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Speaker;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenSpeakerEvent extends OpenItemEvent<Speaker> {
    public OpenSpeakerEvent(Speaker item) {
        super(item);
        setItem(item);
    }
}
