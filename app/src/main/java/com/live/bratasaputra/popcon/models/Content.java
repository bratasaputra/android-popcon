package com.live.bratasaputra.popcon.models;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 5/1/17.
 */

public class Content {
    public final static int ParagraphType = 0;
    public final static int ImageType = 1;
    public final static int FigureCaptionType = 2;

    public Integer type;
    public String imageUrl;
    public String text;

    public static List<Content> CreateContentsFrom(String renderedContent) {
        List<Content> contents = new ArrayList<>();
        Document document = Jsoup.parse(renderedContent);
        Element body = document.body();
        for (Element element : body.children()) {
            switch (element.tagName()) {
                case "p":
                    if (element.select("img").first() != null) {
                        Content newContent = new Content();
                        newContent.type = ImageType;
                        newContent.imageUrl = element.select("img").first().attr("src");
                        contents.add(newContent);
                    } else {
                        String content = element.text().trim();
                        if (content.length() != 0) {
                            Content newContent = new Content();
                            newContent.type = ParagraphType;
                            newContent.text = content;
                            contents.add(newContent);
                        }
                    }
                    break;
                case "figure":
                    Content newContent = new Content();
                    newContent.type = FigureCaptionType;
                    newContent.imageUrl = element.select("img").first().attr("src");
                    newContent.text = element.select("figcaption").first().text();
                    contents.add(newContent);
                    break;
                default:
                    break;
            }
        }
        return contents;
    }
}
