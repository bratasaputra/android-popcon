package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by bratasaputra on 7/28/17.
 */

public class ScheduleGroupedByLocationDeserializer implements JsonDeserializer<ScheduleGroupedByLocation> {
    private static final String[] dateKey = new String[] { "2017-08-04", "2017-08-05", "2017-08-06" };
    private static final String[] dateKeyString = new String[] {"04 August 2017", "05 August 2017", "06 August 2017"};

    @Override
    public ScheduleGroupedByLocation deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ScheduleGroupedByLocation schedule = new ScheduleGroupedByLocation();
        JsonObject jsonObject = json.getAsJsonObject();
        // Deserialise mainStage
        if (jsonObject.has("main-stage") && jsonObject.get("main-stage").isJsonObject()) {
            schedule.mainStage = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("main-stage");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.mainStage.add(dates);
                }
            }
        }
        if (jsonObject.has("workshop") && jsonObject.get("workshop").isJsonObject()) {
            schedule.workshop = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("workshop");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.workshop.add(dates);
                }
            }
        }
        if (jsonObject.has("masterclass") && jsonObject.get("masterclass").isJsonObject()) {
            schedule.masterclass = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("masterclass");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.masterclass.add(dates);
                }
            }
        }
        if (jsonObject.has("parade") && jsonObject.get("parade").isJsonObject()) {
            schedule.parade = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("parade");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.parade.add(dates);
                }
            }
        }
        if(jsonObject.has("pop-connect") && jsonObject.get("pop-connect").isJsonObject()) {
            schedule.popConnect = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("pop-connect");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.popConnect.add(dates);
                }
            }
        }
        if(jsonObject.has("photo-session") && jsonObject.get("photo-session").isJsonObject()) {
            schedule.photoSession = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("photo-session");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.photoSession.add(dates);
                }
            }
        }
        if(jsonObject.has("signing-session") && jsonObject.get("signing-session").isJsonObject()) {
            schedule.signingSession = new ArrayList<>();
            JsonObject dateGroupings = jsonObject.getAsJsonObject("signing-session");
            for(int i = 0; i < dateKey.length; i++) {
                ScheduleGroupedByDates dates = parseDateFrom(dateGroupings, i);
                if(dates != null) {
                    schedule.signingSession.add(dates);
                }
            }
        }
        return schedule;

    }

    private ScheduleGroupedByDates parseDateFrom(JsonObject item, int index) {
        String dateString = dateKey[index];
        if (item.has(dateString) && item.get(dateString).isJsonArray()) {
            ScheduleGroupedByDates dates = new ScheduleGroupedByDates();
            dates.dateString = dateKeyString[index];
            dates.scheduleItems = new ArrayList<>();
            JsonArray scheduleArray = item.getAsJsonArray(dateString);
            for (JsonElement e: scheduleArray) {
                if (e.isJsonObject()) {
                    ScheduleItem scheduleItem = parseItemFrom(e.getAsJsonObject());
                    dates.scheduleItems.add(scheduleItem);
                }
            }
            return dates;
        }
        return null;
    }

    private ScheduleItem parseItemFrom(JsonObject item) {
        ScheduleItem scheduleItem = new ScheduleItem();
        if(item.has("time_start")) {
            scheduleItem.start = item.get("time_start").getAsString();
        }
        if(item.has("time_end")) {
            scheduleItem.end = item.get("time_end").getAsString();
        }
        if(item.has("post_object")) {
            JsonObject postObject = item.getAsJsonObject("post_object");
            if(postObject.has("post_title")) {
                scheduleItem.title = postObject.get("post_title").getAsString();
            }
            if(postObject.has("post_content")) {
                scheduleItem.desc = postObject.get("post_content").getAsString();
            }
            if(postObject.has("ID")) {
                scheduleItem.id = postObject.get("ID").getAsString();
            }
        }
        return scheduleItem;
    }
}
