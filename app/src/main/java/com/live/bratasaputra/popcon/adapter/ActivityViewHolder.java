package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.ActivityItemClickEvent;
import com.live.bratasaputra.popcon.models.Activities;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ActivityViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    private ImageView mImage;
    private TextView mTitle;
    private Activities mActivity;
    private Context mContext;
    private ActivityAdapter.OnItemClickListener mListener = null;

    public ActivityViewHolder(View view, Context glideContext) {
        super(view);
        mContext = glideContext;
        mImage = (ImageView) view.findViewById(R.id.activity_image);
        mTitle = (TextView) view.findViewById(R.id.activity_title);
        view.setOnClickListener(this);
    }

    public void bind(Activities item) {
        mActivity = item;
        mTitle.setText(Jsoup.parse(mActivity.title).text());
        Glide.with(mContext).load(item.featuredMediaUrl).asBitmap().into(mImage);
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) {
            EventBus.getDefault().post(new ActivityItemClickEvent(mActivity));
        } else {
            mListener.OnItemClick(mActivity);
        }
    }

    public void setItemClickListener(ActivityAdapter.OnItemClickListener listener) {
        mListener = listener;
    }
}
