package com.live.bratasaputra.popcon;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.live.bratasaputra.popcon.adapter.SpeakerAdapter;
import com.live.bratasaputra.popcon.models.Speaker;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpeakerListFragment extends Fragment {
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected SpeakerAdapter mAdapter;
    protected ProgressBar mProgressBar;
    private int currentPage = 0;
    private boolean isDownloading = false;
    private int limit = 100;
    private List<Speaker> speakerTempList;
    private Realm realm;
//    private Handler mHandler;

    public SpeakerListFragment() {
        // Required empty public constructor
    }

    public static SpeakerListFragment Create() {
        SpeakerListFragment fragment = new SpeakerListFragment();
        return fragment;
    }

    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    public void loadSpeakers() {
        if (mAdapter.getItemCount() == 0) {
            showLoading();
        }
        isDownloading = true;
        currentPage = currentPage + 1;
        PopCon.getDefaultApiClient().GetSpeakers(currentPage, limit).enqueue(new Callback<List<Speaker>>() {
            @Override
            public void onResponse(Call<List<Speaker>> call, Response<List<Speaker>> response) {
                switch (response.code()) {
                    case 200:
                        List<Speaker> speakers = response.body();
                        if (speakers != null) {
                            speakerTempList.addAll(speakers);
                            if (speakers.size() == limit) {
                                loadSpeakers();
                            } else {
                                realm.beginTransaction();
                                realm.delete(Speaker.class);
                                realm.copyToRealmOrUpdate(speakerTempList);
                                realm.commitTransaction();
                                reloadData();
                            }
                        }
                        break;
                    default:
                        reloadData();
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Speaker>> call, Throwable t) {
                reloadData();
            }
        });
    }

    private void reloadData() {
        isDownloading = false;
        hideLoading();
        RealmResults<Speaker> results = realm.where(Speaker.class).findAll();
        mAdapter.setSpeakers(results);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_speaker_list, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.speaker_list);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SpeakerAdapter(new ArrayList<Speaker>());
        mRecyclerView.setAdapter(mAdapter);
        currentPage = 0;
        limit = 100;
        speakerTempList = new ArrayList<>();
        isDownloading = false;
        realm = Realm.getDefaultInstance();

//        setHasOptionsMenu(true);
        hideLoading();
        return view;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.list_menu, menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.menu_refresh) {
//            if (isDownloading) {
//                Snackbar.make(mRecyclerView, "Please wait...", Snackbar.LENGTH_SHORT).show();
//            } else {
//                isDownloading = false;
//                currentPage = 0;
//                limit = 100;
//                speakerTempList.clear();
//                mAdapter.setSpeakers(new ArrayList<Speaker>());
//                loadSpeakers();
//            }
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        reloadData();

        if (mAdapter.getItemCount() == 0) {
            loadSpeakers();
        }
//        Log.d("POPCON", "On Activity Created");
    }

    @Override
    public void onResume() {
        super.onResume();
//        reloadData();
//
//        if (mAdapter.getItemCount() == 0) {
//            loadSpeakers();
//        }
//        Log.d("POPCON", "On Resume");
    }
}
