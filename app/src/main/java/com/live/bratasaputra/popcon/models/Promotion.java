package com.live.bratasaputra.popcon.models;

/**
 * Created by bratasaputra on 7/31/17.
 */

public class Promotion {
    public Integer id;
    public String title;
    public String content;
    public String featuredMediaUrl;
    public String slug;
}
