package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Exhibitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class ExhibitorAdapter extends RecyclerView.Adapter<ExhibitorViewHolder> {
    private List<Exhibitor> mExhibitors = new ArrayList<>();
    private OnItemClickListener mListener = null;
    private Context mContext = null;

    public ExhibitorAdapter(@NonNull List<Exhibitor> exhibitors, @NonNull Context context) {
        mExhibitors = exhibitors;
        mContext = context;
    }

    public List<Exhibitor> getData() {
        return mExhibitors;
    }

    public void setData(List<Exhibitor> exhibitors) {
        mExhibitors = exhibitors;
        notifyDataSetChanged();
    }

    public void addData(List<Exhibitor> exhibitors) {
        mExhibitors.addAll(exhibitors);
        notifyDataSetChanged();
    }

    @Override
    public ExhibitorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exhibitor_card_list_view, parent, false);
        ExhibitorViewHolder holder = new ExhibitorViewHolder(view, mContext);
        return holder;
    }

    @Override
    public void onBindViewHolder(ExhibitorViewHolder holder, int position) {
        holder.bind(mExhibitors.get(position));
    }

    @Override
    public int getItemCount() {
        return mExhibitors.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void OnItemClick(Exhibitor item);
    }
}
