package com.live.bratasaputra.popcon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.adapter.CompanyViewHolder;
import com.live.bratasaputra.popcon.adapter.ExhibitorViewHolder;
import com.live.bratasaputra.popcon.event.CompanyItemClickEvent;
import com.live.bratasaputra.popcon.event.ExhibitorItemClickEvent;
import com.live.bratasaputra.popcon.event.OpenActivityEvent;
import com.live.bratasaputra.popcon.event.OpenCompanyEvent;
import com.live.bratasaputra.popcon.event.OpenExhibitorEvent;
import com.live.bratasaputra.popcon.models.Activities;
import com.live.bratasaputra.popcon.models.Company;
import com.live.bratasaputra.popcon.models.Exhibitor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public class ActivityDetailActivity extends AppCompatActivity {
    protected Activities mItem;

    protected Toolbar mToolbar;
    protected ImageView mImage;
//    protected TextView mTitle;
//    protected TextView mDescription;
    WebView webView;
    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        OpenActivityEvent event = EventBus.getDefault().removeStickyEvent(OpenActivityEvent.class);
        if (event != null) {
            mItem = event.getItem();

            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, mItem.id);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mItem.title);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Activity");
            analytics = FirebaseAnalytics.getInstance(this);
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mItem.title);

            mImage = (ImageView) findViewById(R.id.activity_image);
            Glide.with(this).load(mItem.featuredMediaUrl).asBitmap().placeholder(R.drawable.placeholder).into(mImage);

            webView = (WebView) findViewById(R.id.activity_desc);
            webView.loadData(mItem.content, "text/html", "utf-8");
            webView.setBackgroundColor(getResources().getColor(android.R.color.transparent));

//            mTitle = (TextView) findViewById(R.id.activity_title);
//            mTitle.setText(mItem.title);
//
//            mDescription = (TextView) findViewById(R.id.activity_desc);
//            Document document = Jsoup.parse(mItem.content);
//            Element element = document.body().children().first();
//            if (element != null) {
//                mDescription.setText(element.text());
//            } else {
//                mDescription.setText(mItem.content);
//            }
        } else {
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void OpenExhibitorDetail(ExhibitorItemClickEvent event) {
        EventBus.getDefault().postSticky(new OpenExhibitorEvent(event.getItem()));
        startActivity(new Intent(this, ExhibitorDetailActivity.class));
    }

    @Subscribe
    public void OpenCompanyDetail(CompanyItemClickEvent event) {
        EventBus.getDefault().postSticky(new OpenCompanyEvent(event.getItem()));
        startActivity(new Intent(this, CompanyDetailActivity.class));
    }

    class ObjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<Object> objects;

        public ObjectAdapter(List<Object> objects) {
            if (objects == null) {
                this.objects = new ArrayList<>();
            } else {
                this.objects = objects;
            }
        }

        public void setAdapter(List<Object> objects) {
            this.objects = objects;
        }

        @Override
        public int getItemViewType(int position) {
            if (objects.get(position).getClass() == Company.class) {
                return 0;
            } else {
                return 1;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            switch (viewType) {
                case 0:
                    return new CompanyViewHolder(inflater.inflate(R.layout.company_card_view, parent, false));
                default:
                    return new ExhibitorViewHolder(inflater.inflate(R.layout.exhibitor_card_view, parent, false), ActivityDetailActivity.this);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (objects.get(position).getClass() == Company.class) {
                ((CompanyViewHolder) holder).bind((Company) objects.get(position));
            } else {
                ((ExhibitorViewHolder) holder).bind((Exhibitor) objects.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return objects.size();
        }
    }
}
