package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by bratasaputra on 6/12/17.
 */

public class ExhibitorCategoryDeserializer implements JsonDeserializer<ExhibitorCategory> {

    @Override
    public ExhibitorCategory deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ExhibitorCategory category = new ExhibitorCategory();
        JsonObject object = json.getAsJsonObject();
        if (object.has("id")) {
            category.id = object.get("id").getAsInt();
        } else {
            return null;
        }
        if (object.has("name")) {
            category.name = object.get("name").getAsString();
        }
        if (object.has("shortname")) {
            category.shortname = object.get("shortname").getAsString();
        }
        if (object.has("desc")) {
            category.desc = object.get("desc").getAsString();
        }
        if (object.has("slug")) {
            category.slug = object.get("slug").getAsString();
        }
        if (object.has("link")) {
            category.link = object.get("link").getAsString();
        }
        return category;
    }
}
