package com.live.bratasaputra.popcon.models;

import java.util.List;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class Company extends Object {
    public Integer id;
    public String link;
    public String title;
    public String content;
    public String featuredMediaUrl;
    public String slug;
    public List<Speaker> speakerList;
    public List<Exhibitor> exhibitorList;
    public List<Activities> activityList;
}
