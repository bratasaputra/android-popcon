package com.live.bratasaputra.popcon.models;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class Exhibitor extends RealmObject {
    @PrimaryKey
    public Integer id;
    public String link;
    public String title;
    public String content;
    public String featuredMediaUrl;
    public String boothNumber;
    public String boothNumber2;
    public String slug;
    public String categories;

    @Ignore
    public List<Speaker> speakerList;
    @Ignore
    public List<Activities> activityList;
    @Ignore
    public List<Company> companyList;
}
