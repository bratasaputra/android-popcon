package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class ActivitiesDeserializer implements JsonDeserializer<Activities> {

    public static Activities DeserializeFrom(JsonObject jsonObject) {
        Activities activities = new Activities();
        if (jsonObject.has("id")) {
            activities.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonPrimitive() && !titleElement.isJsonNull()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                activities.title = titleObject.get("rendered").getAsString();
            }
        } else if (jsonObject.has("post_title")) {
            activities.title = jsonObject.get("post_title").getAsString();
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive() && !contentElement.isJsonNull()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                activities.content = contentObject.get("rendered").getAsString();
            }
        } else if (jsonObject.has("post_content")) {
            activities.content = jsonObject.get("post_content").getAsString();
        }
        if (jsonObject.has("featured_media_url")) {
            activities.featuredMediaUrl = jsonObject.get("featured_media_url").getAsString();
        } else if (jsonObject.has("picture_no_gif")) {
            JsonElement pictureElement = jsonObject.get("picture_no_gif");
            if (pictureElement.isJsonObject()) {
                JsonObject pictureObject = pictureElement.getAsJsonObject();
                if (pictureObject.has("guid")) {
                    activities.featuredMediaUrl = pictureObject.get("guid").getAsString();
                }
            }
        }
        if (jsonObject.has("link")) {
            activities.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("slug")) {
            activities.slug = jsonObject.get("slug").getAsString();
        }
        if (jsonObject.has("tanggal")) {
            activities.tanggal = jsonObject.get("tanggal").getAsString();
        }
        if (jsonObject.has("tanggal_selesai")) {
            activities.tanggalSelesai = jsonObject.get("tanggal_selesai").getAsString();
        }
        if (jsonObject.has("jam_mulai")) {
            activities.jamMulai = jsonObject.get("jam_mulai").getAsString();
        }
        if (jsonObject.has("jam_selesai")) {
            activities.jamSelesai = jsonObject.get("jam_selesai").getAsString();
        }
        if (jsonObject.has("exhibitor")) {
            JsonElement exhibitorElement = jsonObject.get("exhibitor");
            if (exhibitorElement.isJsonArray()) {
                JsonArray exhibitorArray = exhibitorElement.getAsJsonArray();
                activities.exhibitorList = new ArrayList<>();
                for (JsonElement element :
                        exhibitorArray) {
                    JsonObject exhibitorObject = element.getAsJsonObject();
                    Exhibitor exhibitor = ExhibitorDeserializer.DeserializeFrom(exhibitorObject);
                    if (exhibitor != null) {
                        activities.exhibitorList.add(exhibitor);
                    }
                }
            }
        }
        if (jsonObject.has("company")) {
            JsonElement companyElement = jsonObject.get("company");
            if (companyElement.isJsonArray()) {
                JsonArray companyArray = companyElement.getAsJsonArray();
                activities.companyList = new ArrayList<>();
                for (JsonElement element :
                        companyArray) {
                    JsonObject companyObject = element.getAsJsonObject();
                    Company company = CompanyDeserializer.DeserializeFrom(companyObject);
                    if (company != null) {
                        activities.companyList.add(company);
                    }
                }
            }
        }
        if (jsonObject.has("speaker")) {
            JsonElement speakerElement = jsonObject.get("speaker");
            if (speakerElement.isJsonArray()) {
                JsonArray speakerArray = speakerElement.getAsJsonArray();
                activities.speakerList = new ArrayList<>();
                for (JsonElement element :
                        speakerArray) {
                    JsonObject speakerObject = element.getAsJsonObject();
                    Speaker speaker = SpeakerDeserializer.DeserializeFrom(speakerObject);
                    if (speaker != null) {
                        activities.speakerList.add(speaker);
                    }
                }
            }
        }
        return activities;
    }

    @Override
    public Activities deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        return ActivitiesDeserializer.DeserializeFrom(jsonObject);
    }
}
