package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.BlogItemClickEvent;
import com.live.bratasaputra.popcon.models.BlogPost;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class BlogPostViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    private ImageView mImage;
    private TextView mTitle;
    private BlogPost mResponse;
    private Context mContext;
    private BlogPostAdapter.OnItemClickListener mListener = null;

    public BlogPostViewHolder(View view) {
        super(view);
        mContext = view.getContext();
        mImage = (ImageView) view.findViewById(R.id.blog_image);
        mTitle = (TextView) view.findViewById(R.id.blog_title);
        view.setOnClickListener(this);
    }

    public void bind(BlogPost item) {
        this.mResponse = item;
        this.mTitle.setText(Html.fromHtml(item.title));
        Glide.with(mImage.getContext()).load(item.betterFeatureImage).centerCrop().into(mImage);
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) {
            BlogItemClickEvent event = new BlogItemClickEvent(this.mResponse);
            event.mView = mImage;
            EventBus.getDefault().post(event);
        } else
            mListener.OnItemClick(this.mResponse);
    }

    public void setItemClickListener(BlogPostAdapter.OnItemClickListener listener) {
        this.mListener = listener;
    }
}
