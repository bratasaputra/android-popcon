package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Content;

/**
 * Created by bratasaputra on 5/1/17.
 */

public class BlogPostFigureCaptionViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private TextView mText;
    private ImageView mImage;

    public BlogPostFigureCaptionViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mText = (TextView) itemView.findViewById(R.id.figure_caption);
        mImage = (ImageView) itemView.findViewById(R.id.figure_image);
    }

    public void bind(Content content) {
        mText.setText(content.text);
        Glide.with(mImage.getContext()).load(content.imageUrl).placeholder(R.drawable.ic_photo_black).into(mImage);
    }
}
