package com.live.bratasaputra.popcon;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.live.bratasaputra.popcon.adapter.BlogPostAdapter;
import com.live.bratasaputra.popcon.models.BlogPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlogListFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, RecyclerView.OnChildAttachStateChangeListener {
    protected RecyclerView mRecyclerView = null;
    protected RecyclerView.LayoutManager mLayoutManager = null;
    protected BlogPostAdapter mAdapter = null;
    protected ProgressBar mProgressBar = null;
    protected ProgressBar mHorizontalBar = null;
    protected SwipeRefreshLayout mRefreshLayout = null;

    protected boolean isLoading = false;
    protected PopConApi mClient = PopCon.getDefaultApiClient();
    protected int page = 1;
    protected boolean lastPage = false;
    protected int limit = 10;

    protected Callback<List<BlogPost>> loadInitialList = new Callback<List<BlogPost>>() {
        @Override
        public void onResponse(Call<List<BlogPost>> call, Response<List<BlogPost>> response) {
            switch (response.code()) {
                case 200:
                    List<BlogPost> body = response.body();
                    if (body != null && !body.isEmpty()) {
                        if (body.size() < limit) {
                            lastPage = true;
                        }
                        mAdapter.setData(body);
                    } else {
                        lastPage = true;
                    }
                    break;
            }
            doneLoading();
        }

        @Override
        public void onFailure(Call<List<BlogPost>> call, Throwable t) {
            doneLoading();
        }
    };

    protected Callback<List<BlogPost>> loadNextPage = new Callback<List<BlogPost>>() {
        @Override
        public void onResponse(Call<List<BlogPost>> call, Response<List<BlogPost>> response) {
            switch (response.code()) {
                case 200:
                    List<BlogPost> body = response.body();
                    if (body != null && !body.isEmpty()) {
                        if (body.size() < limit) {
                            lastPage = true;
                        }
                        List<BlogPost> list = mAdapter.getData();
                        int count = list.size();
                        list.addAll(body);
                        mAdapter.setData(list);
                        if (list.size() > count) {
                            mRecyclerView.smoothScrollToPosition(count);
                        }
                    } else {
                        lastPage = true;
                    }
                    break;
            }
            doneLoading();
        }

        @Override
        public void onFailure(Call<List<BlogPost>> call, Throwable t) {
            doneLoading();
        }
    };

    public BlogListFragment() {
        // Required empty public constructor
    }

    public static BlogListFragment Create() {
        BlogListFragment fragment = new BlogListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blog_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.blog_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BlogPostAdapter(null);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnChildAttachStateChangeListener(this);

        mProgressBar = (ProgressBar) view.findViewById(R.id.blogs_load_progress);
        mHorizontalBar = (ProgressBar) view.findViewById(R.id.horizontal_progress);
        mHorizontalBar.setVisibility(View.INVISIBLE);

        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);

        mClient.GetPosts().enqueue(loadInitialList);
        isLoading = true;
        mProgressBar.setVisibility(View.VISIBLE);
        mRefreshLayout.setVisibility(View.INVISIBLE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void doneLoading() {
        isLoading = false;
        mRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
        mHorizontalBar.setVisibility(View.INVISIBLE);
        if (mAdapter.getItemCount() == 0) {
            mRefreshLayout.setVisibility(View.INVISIBLE);
        } else {
            mRefreshLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            mRefreshLayout.setRefreshing(false);
            return;
        }
        this.page = 1;
        lastPage = false;
        mClient.GetPosts().enqueue(loadInitialList);
        mRefreshLayout.setRefreshing(true);
        isLoading = true;
    }

    @Override
    public void onChildViewAttachedToWindow(View view) {
        int position = mRecyclerView.findContainingViewHolder(view).getAdapterPosition();
        if (position == mAdapter.getItemCount() - 1 && !lastPage && !isLoading) {
            shouldFetchNextPosts();
        }
    }

    @Override
    public void onChildViewDetachedFromWindow(View view) {

    }

    public void shouldFetchNextPosts() {
        if (!lastPage) {
            int currentPage = (mAdapter.getItemCount() / limit) + 1;
            mClient.GetPosts(currentPage, limit).enqueue(loadNextPage);
            isLoading = true;
            mHorizontalBar.setVisibility(View.VISIBLE);
        }
    }
}
