package com.live.bratasaputra.popcon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Content;

/**
 * Created by bratasaputra on 5/1/17.
 */

public class BlogPostParagraphViewHolder extends RecyclerView.ViewHolder {
    private TextView mText;

    public BlogPostParagraphViewHolder(View itemView) {
        super(itemView);
        mText = (TextView) itemView.findViewById(R.id.text);
    }

    public void bind(Content content) {
        mText.setText(content.text);
    }
}
