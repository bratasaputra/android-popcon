package com.live.bratasaputra.popcon.event;

import android.widget.ImageView;

import com.live.bratasaputra.popcon.models.Speaker;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class SpeakerItemClickEvent extends ItemClickEvent<Speaker> {
    public ImageView mView;

    public SpeakerItemClickEvent(Speaker item) {
        super(item);
        setItem(item);
    }
}
