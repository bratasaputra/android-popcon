package com.live.bratasaputra.popcon;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.live.bratasaputra.popcon.adapter.ActivityAdapter;
import com.live.bratasaputra.popcon.adapter.BlogPostAdapter;
import com.live.bratasaputra.popcon.models.Activities;
import com.live.bratasaputra.popcon.models.BlogPost;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByLocation;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private ImageView mBanner;
    private RecyclerView mBlogListView;
//    private RecyclerView mActivityListView;
    private RecyclerView.LayoutManager mBlogLayoutManager;
//    private RecyclerView.LayoutManager mActivityLayoutManager;
    private BlogPostAdapter mPostAdapter;
//    private ActivityAdapter mActivityAdapter;
    private ProgressBar progressBar;
    private View content;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment Create() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mBanner = (ImageView) view.findViewById(R.id.banner);
        mBlogListView = (RecyclerView) view.findViewById(R.id.blogs);
//        mActivityListView = (RecyclerView) view.findViewById(R.id.activities);
        mBlogLayoutManager = new LinearLayoutManager(getContext());
//        mActivityLayoutManager = new LinearLayoutManager(getContext());
        mBlogListView.setLayoutManager(mBlogLayoutManager);
//        mActivityListView.setLayoutManager(mActivityLayoutManager);
        mPostAdapter = new BlogPostAdapter(null);
        mBlogListView.setAdapter(mPostAdapter);
//        mActivityAdapter = new ActivityAdapter(new ArrayList<Activities>(), getContext());
//        mActivityListView.setAdapter(mActivityAdapter);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        content = view.findViewById(R.id.home_content);
        content.setVisibility(View.GONE);

        view.findViewById(R.id.buyTicket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(getContext());
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, 1);
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Buy Popcon Ticket");
                bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "Ticket");
                analytics.logEvent(FirebaseAnalytics.Event.PRESENT_OFFER, bundle);

                String url = "https://www.kiostix.com/popconasia";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(getContext(), Uri.parse(url));
            }
        });

        ((Button) view.findViewById(R.id.moreNews)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), BlogListActivity.class));
            }
        });

        PopCon.getDefaultApiClient().GetPosts(1, 10).enqueue(new Callback<List<BlogPost>>() {
            @Override
            public void onResponse(Call<List<BlogPost>> call, Response<List<BlogPost>> response) {
                switch (response.code()) {
                    case 200:
                        mPostAdapter.setData(response.body());
                        progressBar.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<BlogPost>> call, Throwable t) {

            }
        });
        return view;
    }
}
