package com.live.bratasaputra.popcon;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.adapter.ContentAdapter;
import com.live.bratasaputra.popcon.event.OpenBlogEvent;
import com.live.bratasaputra.popcon.models.BlogPost;
import com.live.bratasaputra.popcon.models.Content;

import org.greenrobot.eventbus.EventBus;

public class BlogDetailActivity extends AppCompatActivity {
    protected BlogPost mItem;

    protected Toolbar mToolbar;
    protected ImageView mImage;
    protected TextView mTitle;
    protected RecyclerView mContent;
    protected RecyclerView.LayoutManager mLayout;
    protected ContentAdapter mAdapter;
    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);

        OpenBlogEvent event = EventBus.getDefault().removeStickyEvent(OpenBlogEvent.class);
        if (event != null) {
            this.mItem = event.getItem();

            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, mItem.id);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mItem.title);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "BlogPost");
            analytics = FirebaseAnalytics.getInstance(this);
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            mImage = (ImageView) findViewById(R.id.blog_image);
            Glide.with(mImage.getContext()).load(mItem.betterFeatureImage).into(mImage);

            mTitle = (TextView) findViewById(R.id.blog_title);
            mTitle.setText(Html.fromHtml(mItem.title));

            mContent = (RecyclerView) findViewById(R.id.blog_content);
            mLayout = new LinearLayoutManager(this);
            mContent.setLayoutManager(mLayout);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blog_detail_menu, menu);
        menu.findItem(R.id.menu_share).getIcon().setTint(Color.WHITE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.menu_share:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAdapter = new ContentAdapter(Content.CreateContentsFrom(mItem.content));
        mContent.setAdapter(mAdapter);
    }
}
