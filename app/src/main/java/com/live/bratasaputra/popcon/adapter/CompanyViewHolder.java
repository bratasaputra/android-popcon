package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.PopCon;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.CompanyItemClickEvent;
import com.live.bratasaputra.popcon.models.Company;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bratasaputra on 5/2/17.
 */

public class CompanyViewHolder extends RecyclerView.ViewHolder {
    private Company mCompany;
    private ImageView mImage;
    private TextView mName;
    private Context mContext;

    public CompanyViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mImage = (ImageView) itemView.findViewById(R.id.company_image);
        mName = (TextView) itemView.findViewById(R.id.company_name);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new CompanyItemClickEvent(mCompany));
            }
        });
    }

    public void bind(Company company) {
        this.mCompany = company;
        mName.setText(mCompany.title);
        if (mCompany.featuredMediaUrl != null) {
            Glide.with(mContext).load(mCompany.featuredMediaUrl).into(mImage);
        } else {
            PopCon.getDefaultApiClient().GetCompany(mCompany.id).enqueue(new Callback<Company>() {
                @Override
                public void onResponse(Call<Company> call, Response<Company> response) {
                    switch (response.code()) {
                        case 200:
                            mCompany = response.body();
                            Glide.with(mContext).load(mCompany.featuredMediaUrl).into(mImage);
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onFailure(Call<Company> call, Throwable t) {

                }
            });
        }
    }
}
