package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Activities;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenActivityEvent extends OpenItemEvent<Activities> {
    public OpenActivityEvent(Activities item) {
        super(item);
        setItem(item);
    }
}
