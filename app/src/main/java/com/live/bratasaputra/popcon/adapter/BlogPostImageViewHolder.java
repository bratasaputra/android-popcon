package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Content;

/**
 * Created by bratasaputra on 5/1/17.
 */

public class BlogPostImageViewHolder extends RecyclerView.ViewHolder {
    private ImageView mImage;
    private Context mContext;

    public BlogPostImageViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mImage = (ImageView) itemView.findViewById(R.id.image);
    }

    public void bind(Content content) {
        Glide.with(mImage.getContext()).load(content.imageUrl).into(mImage);
    }
}
