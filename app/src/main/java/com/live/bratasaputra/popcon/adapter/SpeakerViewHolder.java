package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.event.SpeakerItemClickEvent;
import com.live.bratasaputra.popcon.models.Speaker;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

/**
 * Created by bratasaputra on 5/13/17.
 */

public class SpeakerViewHolder extends RecyclerView.ViewHolder {
    private Speaker mSpeaker;
    private ImageView mImage;
    private TextView mTitle;
    private TextView mInfo;
    private Context mContext;

    public SpeakerViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mImage = (ImageView) itemView.findViewById(R.id.speaker_image);
        mTitle = (TextView) itemView.findViewById(R.id.speaker_title);
        mInfo = (TextView) itemView.findViewById(R.id.speaker_info);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SpeakerItemClickEvent(mSpeaker));
            }
        });
    }

    public void bind(Speaker speaker) {
        this.mSpeaker = speaker;
        Glide.with(mImage.getContext()).load(mSpeaker.featuredMediaUrl).into(mImage);
        mTitle.setText(mSpeaker.title);
        mInfo.setText(String.format(Locale.getDefault(), "%s, %s", mSpeaker.jabatan, mSpeaker.organisasi));
    }
}
