package com.live.bratasaputra.popcon.event;

/**
 * Created by bratasaputra on 8/1/17.
 */

public class OpenScheduleEvent {
    public final SCHEDULENAME schedulename;

    public OpenScheduleEvent(SCHEDULENAME name) {
        schedulename = name;
    }
}

