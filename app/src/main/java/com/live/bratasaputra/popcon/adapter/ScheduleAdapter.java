package com.live.bratasaputra.popcon.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Schedule;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByDates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 5/4/17.
 */

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleViewHolder> {
    protected List<ScheduleGroupedByDates> schedules = new ArrayList<>();

    public ScheduleAdapter(@NonNull List<ScheduleGroupedByDates> schedules) {
        if (schedules != null) {
            this.schedules = schedules;
        }
    }

    public List<ScheduleGroupedByDates> getData() {
        return this.schedules;
    }

    public void setData(@NonNull List<ScheduleGroupedByDates> schedules) {
        this.schedules = schedules;
        notifyDataSetChanged();
    }

    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_card_view, parent, false);
        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, int position) {
        holder.bind(schedules.get(position));
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }
}
