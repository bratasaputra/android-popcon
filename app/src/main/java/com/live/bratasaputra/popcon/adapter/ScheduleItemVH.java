package com.live.bratasaputra.popcon.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.ScheduleItem;

/**
 * Created by bratasaputra on 8/1/17.
 */

public class ScheduleItemVH extends RecyclerView.ViewHolder {
    ScheduleItem mItem;
    TextView mTime;
    TextView mTitle;
    TextView mDesc;

    public ScheduleItemVH(View view) {
        super(view);
        mTime = view.findViewById(R.id.item_time);
        mTitle = view.findViewById(R.id.item_title);
        mDesc = view.findViewById(R.id.item_name);
    }

    public void bind(ScheduleItem item) {
        this.mItem = item;
        mTime.setText(String.format("%s - %s", mItem.start, mItem.end));
        mTitle.setText(mItem.title);
        if(TextUtils.isEmpty(mItem.desc)) {
            mDesc.setVisibility(View.GONE);
        } else {
            mDesc.setText(mItem.desc);
            mDesc.setVisibility(View.VISIBLE);
        }
    }
}
