package com.live.bratasaputra.popcon;

import com.live.bratasaputra.popcon.models.Activities;
import com.live.bratasaputra.popcon.models.BlogPost;
import com.live.bratasaputra.popcon.models.Company;
import com.live.bratasaputra.popcon.models.Exhibitor;
import com.live.bratasaputra.popcon.models.ExhibitorCategory;
import com.live.bratasaputra.popcon.models.Promotion;
import com.live.bratasaputra.popcon.models.Schedule;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByLocation;
import com.live.bratasaputra.popcon.models.Speaker;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by bratasaputra on 2/19/17.
 */

public interface PopConApi {
    @GET("wp-json/wp/v2/posts/{id}")
    Call<BlogPost> GetPost(@Path("id") int postId);

    @GET("wp-json/wp/v2/posts")
    Call<List<BlogPost>> GetPosts();

    @GET("wp-json/wp/v2/posts")
    Call<List<BlogPost>> GetPosts(@Query("page") int page, @Query("per_page") int limit);

    @GET("wp-json/wp/v2/activities/{id}")
    Call<Activities> GetActivity(@Path("id") int postId);

    @GET("wp-json/wp/v2/activities")
    Call<List<Activities>> GetActivities();

    @GET("wp-json/wp/v2/activities")
    Call<List<Activities>> GetActivities(@Query("page") int page, @Query("per_page") int limit);

    @GET("wp-json/wp/v2/exhibitor")
    Call<List<Exhibitor>> GetExhibitors();

    @GET("wp-json/wp/v2/exhibitor/{id}")
    Call<Exhibitor> GetExhibitors(@Path("id") int exhibitorId);

    @GET("wp-json/wp/v2/exhibitor_categories")
    Call<List<ExhibitorCategory>> GetExhibitorCategory(@Query("per_page") int limit);

    @GET("wp-json/wp/v2/exhibitor")
    Call<List<Exhibitor>> GetExhibitors(@Query("page") int page, @Query("per_page") int limit);

    @GET("wp-json/wp/v2/company/{id}")
    Call<Company> GetCompany(@Path("id") int companyId);

    @GET("wp-json/wp/v2/speaker")
    Call<List<Speaker>> GetSpeakers();

    @GET("wp-json/wp/v2/speaker")
    Call<List<Speaker>> GetSpeakers(@Query("page") int page, @Query("per_page") int limit);

    @GET("wp-json/wp/v2/promotion")
    Call<List<Promotion>> GetPromotions(@Query("per_page") int limit);

    @GET("wp-json/wp/v2/schedule_composed")
    Call<ScheduleGroupedByLocation> GetSchedule();
}
