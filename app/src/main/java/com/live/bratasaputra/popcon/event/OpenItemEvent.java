package com.live.bratasaputra.popcon.event;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenItemEvent<T> extends BaseEvent<T> {
    public OpenItemEvent(T item) {
        super(item);
        this.mItem = item;
    }
}
