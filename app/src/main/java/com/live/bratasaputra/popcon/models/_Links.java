package com.live.bratasaputra.popcon.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class _Links {
    @SerializedName("self")
    public Self[] self;
    @SerializedName("collection")
    public Collection[] collection;
    @SerializedName("about")
    public About[] about;
    @SerializedName("author")
    public Author[] author;
    @SerializedName("replies")
    public Replies[] replies;
    @SerializedName("version-history")
    public VersionHistory[] versionHistory;
    @SerializedName("wp:featuredmedia")
    public WpFeaturedMedia[] wpFeaturedMedia;
    @SerializedName("wp:attachment")
    public WpAttachment[] wpAttachment;
    @SerializedName("wp:term")
    public WpTerm[] wpTerm;
    @SerializedName("curies")
    public Curies[] curies;

    public class Self {
        @SerializedName("href")
        public String href;
    }

    public class Collection {
        @SerializedName("href")
        public String href;
    }

    public class About {
        @SerializedName("href")
        public String href;
    }

    public class Author {
        @SerializedName("embeddable")
        public Boolean embeddable;
        @SerializedName("href")
        public String href;
    }

    public class Replies {
        @SerializedName("embeddable")
        public Boolean embeddable;
        @SerializedName("href")
        public String href;
    }

    public class VersionHistory {
        @SerializedName("href")
        public String href;
    }

    public class WpFeaturedMedia {
        @SerializedName("embeddable")
        public Boolean embeddable;
        @SerializedName("href")
        public String href;
    }

    public class WpAttachment {
        @SerializedName("href")
        public String href;
    }

    public class WpTerm {
        @SerializedName("taxonomy")
        public String taxonomy;
        @SerializedName("embeddable")
        public Boolean embeddable;
        @SerializedName("href")
        public String href;
    }

    public class Curies {
        @SerializedName("name")
        public String name;
        @SerializedName("href")
        public String href;
        @SerializedName("templated")
        public Boolean templated;
    }

}
