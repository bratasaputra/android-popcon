package com.live.bratasaputra.popcon;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.live.bratasaputra.popcon.adapter.ScheduleAdapter;
import com.live.bratasaputra.popcon.event.OpenScheduleEvent;
import com.live.bratasaputra.popcon.event.SCHEDULENAME;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByDates;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduleFragment extends Fragment {
    Button mainStage;
    Button popconnect;
    Button workshop;
    Button parade;
    Button masterclass;

    public ScheduleFragment() {
        // Required empty public constructor
    }

    public static ScheduleFragment newInstance() {
        ScheduleFragment fragment = new ScheduleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        mainStage = view.findViewById(R.id.main_stage);
        mainStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.MAINSTAGE));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        masterclass = view.findViewById(R.id.masterclass);
        masterclass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.MASTERCLASS));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        popconnect = view.findViewById(R.id.pop_connect);
        popconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.POPCONNECT));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        parade = view.findViewById(R.id.parade);
        parade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.PARADE));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        workshop = view.findViewById(R.id.workshop);
        workshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.WORKSHOP));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        view.findViewById(R.id.photo_session).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.PHOTO_SESSION));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        view.findViewById(R.id.signing_session).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new OpenScheduleEvent(SCHEDULENAME.SIGNING_SESSION));
                startActivity(new Intent(getContext(), ScheduleListActivity.class));
            }
        });
        return view;
    }
}
