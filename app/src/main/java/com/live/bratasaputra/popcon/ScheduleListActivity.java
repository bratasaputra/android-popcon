package com.live.bratasaputra.popcon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.live.bratasaputra.popcon.event.OpenScheduleEvent;

import org.greenrobot.eventbus.EventBus;

public class ScheduleListActivity extends AppCompatActivity {
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_list);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        OpenScheduleEvent event = EventBus.getDefault().getStickyEvent(OpenScheduleEvent.class);
        if(event != null) {
            switch (event.schedulename) {
                case MAINSTAGE:
                    setTitle("Main Stage");
                    break;
                case PARADE:
                    setTitle("Parade");
                    break;
                case POPCONNECT:
                    setTitle("Pop Connect");
                    break;
                case MASTERCLASS:
                    setTitle("Masterclass");
                    break;
                case WORKSHOP:
                    setTitle("Workshop");
                    break;
                case PHOTO_SESSION:
                    setTitle("Photo Session");
                    break;
                case SIGNING_SESSION:
                    setTitle("Signing Session");
                    break;
            }
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
