package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Company;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class CompanyItemClickEvent extends ItemClickEvent<Company> {
    public CompanyItemClickEvent(Company item) {
        super(item);
        setItem(item);
    }
}
