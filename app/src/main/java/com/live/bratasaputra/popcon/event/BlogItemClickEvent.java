package com.live.bratasaputra.popcon.event;

import android.widget.ImageView;

import com.live.bratasaputra.popcon.models.BlogPost;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class BlogItemClickEvent extends ItemClickEvent<BlogPost> {
    public ImageView mView;

    public BlogItemClickEvent(BlogPost item) {
        super(item);
        setItem(item);
    }
}
