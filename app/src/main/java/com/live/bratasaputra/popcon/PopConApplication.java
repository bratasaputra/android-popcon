package com.live.bratasaputra.popcon;

import android.app.Application;

/**
 * Created by bratasaputra on 6/12/17.
 */

public class PopConApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        PopCon.init(getApplicationContext());
    }
}
