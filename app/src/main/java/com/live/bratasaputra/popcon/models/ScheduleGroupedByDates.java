package com.live.bratasaputra.popcon.models;

import java.util.List;

public class ScheduleGroupedByDates {
    public String dateString;
    public List<ScheduleItem> scheduleItems;
}
