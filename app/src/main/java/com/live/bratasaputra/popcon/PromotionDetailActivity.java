package com.live.bratasaputra.popcon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.event.OpenPromotionEvent;

import org.greenrobot.eventbus.EventBus;

public class PromotionDetailActivity extends AppCompatActivity {
    Toolbar mToolbar;
    WebView desc;
    ImageView imageView;
    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_detail);

        mToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        desc = findViewById(R.id.promotion_desc);
        desc.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        imageView = findViewById(R.id.promotion_image);
        OpenPromotionEvent event = EventBus.getDefault().removeStickyEvent(OpenPromotionEvent.class);
        if(event != null) {
            setTitle(event.promotion.title);

            Glide.with(this).load(event.promotion.featuredMediaUrl).into(imageView);
            desc.loadData(event.promotion.content, "text/html", "utf-8");

            analytics = FirebaseAnalytics.getInstance(this);
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, event.promotion.id);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event.promotion.title);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Promotion");
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
