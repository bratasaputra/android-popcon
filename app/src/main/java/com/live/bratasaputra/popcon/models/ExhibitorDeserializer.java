package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.live.bratasaputra.popcon.PopCon;

import java.lang.reflect.Type;

import io.realm.Realm;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class ExhibitorDeserializer implements JsonDeserializer<Exhibitor> {

    public static Exhibitor DeserializeFrom(JsonObject jsonObject) {
        Exhibitor exhibitor = new Exhibitor();
        Realm realm = Realm.getDefaultInstance();
        String categoryShortname = "";
        if (jsonObject.has("id")) {
            exhibitor.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("slug")) {
            exhibitor.slug = jsonObject.get("slug").getAsString();
        } else if (jsonObject.has("post_name")) {
            exhibitor.slug = jsonObject.get("post_name").getAsString();
        }
        if (jsonObject.has("link")) {
            exhibitor.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("featured_media_url")) {
            exhibitor.featuredMediaUrl = jsonObject.get("featured_media_url").getAsString();
        }
        if (jsonObject.has("exhibitor_categories")) {
            try {
                JsonArray categories = jsonObject.get("exhibitor_categories").getAsJsonArray();
                for (JsonElement e :
                        categories) {
                    int cat = e.getAsInt();
                    ExhibitorCategory exhibitorCategory = realm.where(ExhibitorCategory.class).equalTo("id", cat).findFirst();
                    exhibitor.categories = exhibitorCategory.name;
                    categoryShortname = exhibitorCategory.shortname;
                    break;
                }
            } catch (Exception ex) {
                PopCon.print(ex.toString());
            }
        }
        if (jsonObject.has("booth_number")) {
            exhibitor.boothNumber = String.format("%s.%s", categoryShortname, jsonObject.get("booth_number").getAsString());
        }
        if (jsonObject.has("booth_number_2") && jsonObject.get("booth_number_2").isJsonPrimitive()) {
            String temp = jsonObject.get("booth_number_2").getAsString();
            temp = temp.trim();
            if (!temp.equals("0")) {
                exhibitor.boothNumber2 = String.format("%s.%s", categoryShortname, temp);
            } else {
                exhibitor.boothNumber2 = null;
            }
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonNull() && !titleElement.isJsonPrimitive()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                if (titleObject.has("rendered")) {
                    exhibitor.title = titleObject.get("rendered").getAsString();
                }
            }
        } else if (jsonObject.has("post_title")) {
            exhibitor.title = jsonObject.get("post_title").getAsString();
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive() && !contentElement.isJsonNull()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                if (contentObject.has("rendered")) {
                    exhibitor.content = contentObject.get("rendered").getAsString();
                }
            } else if (jsonObject.has("post_content")) {
                exhibitor.content = jsonObject.get("post_content").getAsString();
            }
        }
        return exhibitor;
    }

    @Override
    public Exhibitor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return ExhibitorDeserializer.DeserializeFrom(jsonObject);
    }
}
