package com.live.bratasaputra.popcon.models;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class Activities extends RealmObject {
    @PrimaryKey
    public Integer id;
    public String link;
    public String slug;
    public String title;
    public String content;
    public String featuredMediaUrl;
    public String tanggal;
    public String tanggalSelesai;
    public String jamMulai;
    public String jamSelesai;

    @Ignore
    public List<Exhibitor> exhibitorList;
    @Ignore
    public List<Company> companyList;
    @Ignore
    public List<Speaker> speakerList;
}
