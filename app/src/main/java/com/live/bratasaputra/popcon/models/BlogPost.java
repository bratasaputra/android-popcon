package com.live.bratasaputra.popcon.models;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class BlogPost {
    public Integer id;
    public String link;
    public String slug;
    public String title;
    public String content;
    public String betterFeatureImage;
}
