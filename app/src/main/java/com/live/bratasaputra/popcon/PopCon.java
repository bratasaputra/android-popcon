package com.live.bratasaputra.popcon;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.live.bratasaputra.popcon.models.Activities;
import com.live.bratasaputra.popcon.models.ActivitiesDeserializer;
import com.live.bratasaputra.popcon.models.BlogPost;
import com.live.bratasaputra.popcon.models.BlogPostDeserializer;
import com.live.bratasaputra.popcon.models.Company;
import com.live.bratasaputra.popcon.models.CompanyDeserializer;
import com.live.bratasaputra.popcon.models.Exhibitor;
import com.live.bratasaputra.popcon.models.ExhibitorCategory;
import com.live.bratasaputra.popcon.models.ExhibitorCategoryDeserializer;
import com.live.bratasaputra.popcon.models.ExhibitorDeserializer;
import com.live.bratasaputra.popcon.models.Promotion;
import com.live.bratasaputra.popcon.models.PromotionDeserializer;
import com.live.bratasaputra.popcon.models.Schedule;
import com.live.bratasaputra.popcon.models.ScheduleDeserializer;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByLocation;
import com.live.bratasaputra.popcon.models.ScheduleGroupedByLocationDeserializer;
import com.live.bratasaputra.popcon.models.Speaker;
import com.live.bratasaputra.popcon.models.SpeakerDeserializer;

import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class PopCon {
    private static PopConApi client = null;
    private static boolean isInitialised = false;
    private static Gson gson = null;

    public static PopConApi getDefaultApiClient() {
        if (client == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://popconasia.com/")
                    .addConverterFactory(GsonConverterFactory.create(getDefaultGson()))
                    .build();

            client = retrofit.create(PopConApi.class);
        }
        return client;
    }

    public static Gson getDefaultGson() {
        if (gson == null) {
            BlogPostDeserializer blogPostDeserializer = new BlogPostDeserializer();
            ActivitiesDeserializer activitiesDeserializer = new ActivitiesDeserializer();
            ExhibitorDeserializer exhibitorDeserializer = new ExhibitorDeserializer();
            CompanyDeserializer companyDeserializer = new CompanyDeserializer();
            ScheduleDeserializer scheduleDeserializer = new ScheduleDeserializer();
            SpeakerDeserializer speakerDeserializer = new SpeakerDeserializer();
            ExhibitorCategoryDeserializer exhibitorCategoryDeserializer = new ExhibitorCategoryDeserializer();
            ScheduleGroupedByLocationDeserializer scheduleGroupedByLocationDeserializer = new ScheduleGroupedByLocationDeserializer();
            PromotionDeserializer promotionDeserializer = new PromotionDeserializer();

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(BlogPost.class, blogPostDeserializer);
            gsonBuilder.registerTypeAdapter(Activities.class, activitiesDeserializer);
            gsonBuilder.registerTypeAdapter(Exhibitor.class, exhibitorDeserializer);
            gsonBuilder.registerTypeAdapter(Company.class, companyDeserializer);
            gsonBuilder.registerTypeAdapter(Schedule.class, scheduleDeserializer);
            gsonBuilder.registerTypeAdapter(Speaker.class, speakerDeserializer);
            gsonBuilder.registerTypeAdapter(ExhibitorCategory.class, exhibitorCategoryDeserializer);
            gsonBuilder.registerTypeAdapter(ScheduleGroupedByLocation.class, scheduleGroupedByLocationDeserializer);
            gsonBuilder.registerTypeAdapter(Promotion.class, promotionDeserializer);

            gson = gsonBuilder.create();
        }
        return gson;
    }

    public static void init(Context context) {
        if (!isInitialised) {
            // Do init here
            Realm.init(context);
            isInitialised = true;
        }
    }

    public static void print(String text) {
        Log.d("POPCON", text);
    }
}
