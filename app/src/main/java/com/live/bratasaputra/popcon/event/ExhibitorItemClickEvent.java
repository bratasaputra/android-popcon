package com.live.bratasaputra.popcon.event;

import android.widget.ImageView;

import com.live.bratasaputra.popcon.models.Exhibitor;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ExhibitorItemClickEvent extends ItemClickEvent<Exhibitor> {
    public ImageView mView;

    public ExhibitorItemClickEvent(Exhibitor item) {
        super(item);
        setItem(item);
    }
}
