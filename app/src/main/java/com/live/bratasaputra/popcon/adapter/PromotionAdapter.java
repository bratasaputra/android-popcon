package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Promotion;

import java.util.List;

/**
 * Created by bratasaputra on 7/31/17.
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionViewHolder> {
    List<Promotion> promotions;
    Context mContext;

    public PromotionAdapter(@NonNull List<Promotion> promotions, Context fragment) {
        mContext = fragment;
        this.promotions = promotions;
    }

    public void setPromotions(@NonNull List<Promotion> promotions) {
        this.promotions = promotions;
        notifyDataSetChanged();
    }

    @Override
    public PromotionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.promotion_card, parent, false);
        return new PromotionViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(PromotionViewHolder holder, int position) {
        holder.bindViewHolder(promotions.get(position));
    }

    @Override
    public int getItemCount() {
        return promotions.size();
    }
}
