package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by bratasaputra on 7/31/17.
 */

public class PromotionDeserializer implements JsonDeserializer<Promotion> {

    @Override
    public Promotion deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Promotion promotion = new Promotion();
        JsonObject jsonObject = json.getAsJsonObject();
        if(jsonObject.has("id")) {
            promotion.id = jsonObject.get("id").getAsInt();
        }
        if(jsonObject.has("slug")) {
            promotion.slug = jsonObject.get("slug").getAsString();
        }
        if(jsonObject.has("title") && jsonObject.get("title").isJsonObject()) {
            JsonObject titleObject = jsonObject.get("title").getAsJsonObject();
            if(titleObject.has("rendered")) {
                promotion.title = titleObject.get("rendered").getAsString();
            }
        }
        if(jsonObject.has("content") && jsonObject.get("content").isJsonObject()) {
            JsonObject contentObject = jsonObject.get("content").getAsJsonObject();
            if(contentObject.has("rendered")) {
                promotion.content = contentObject.get("rendered").getAsString();
            }
        }
        if(jsonObject.has("featured_media_url")) {
            promotion.featuredMediaUrl = jsonObject.get("featured_media_url").getAsString();
        }
        return promotion;
    }
}
