package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Activities;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ActivityItemClickEvent extends ItemClickEvent<Activities> {
    public ActivityItemClickEvent(Activities item) {
        super(item);
        setItem(item);
    }
}
