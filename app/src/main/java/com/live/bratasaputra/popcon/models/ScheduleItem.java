package com.live.bratasaputra.popcon.models;

public class ScheduleItem {
    public String id;
    public String title;
    public String desc;
    public String start;
    public String end;
}
