package com.live.bratasaputra.popcon.models;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class Speaker extends RealmObject {
    @PrimaryKey
    public Integer id;
    public String slug;
    public String link;
    public String links;
    public String title;
    public String content;
    public String featuredMediaUrl;
    public String jabatan;
    public String organisasi;

    @Ignore
    public List<Activities> activityList;
    @Ignore
    public List<Exhibitor> exhibitorList;
    @Ignore
    public List<Company> companyList;
}
