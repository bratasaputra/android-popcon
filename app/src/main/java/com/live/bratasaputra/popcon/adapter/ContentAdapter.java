package com.live.bratasaputra.popcon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Content;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 5/1/17.
 */

public class ContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Content> contents = new ArrayList<>();

    public ContentAdapter(List<Content> contents) {
        if (contents != null) {
            this.contents = contents;
        }
    }

    public void setContents(List<Content> contents) {
        if (contents != null) {
            this.contents = contents;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return this.contents.get(position).type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Content.ParagraphType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.paragraph_layout, parent, false);
                return new BlogPostParagraphViewHolder(view);
            case Content.FigureCaptionType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.figure_layout, parent, false);
                return new BlogPostFigureCaptionViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_layout, parent, false);
                return new BlogPostImageViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Content content = this.contents.get(position);
        switch (content.type) {
            case Content.ParagraphType:
                ((BlogPostParagraphViewHolder) holder).bind(content);
                break;
            case Content.FigureCaptionType:
                ((BlogPostFigureCaptionViewHolder) holder).bind(content);
                break;
            default:
                ((BlogPostImageViewHolder) holder).bind(content);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (this.contents != null) {
            return this.contents.size();
        }
        return 0;
    }
}
