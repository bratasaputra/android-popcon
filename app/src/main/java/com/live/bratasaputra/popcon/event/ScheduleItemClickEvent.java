package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Schedule;

/**
 * Created by bratasaputra on 3/1/17.
 */

public class ScheduleItemClickEvent extends ItemClickEvent<Schedule> {
    public ScheduleItemClickEvent(Schedule item) {
        super(item);
        setItem(item);
    }
}
