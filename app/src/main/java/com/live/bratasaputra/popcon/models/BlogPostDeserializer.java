package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class BlogPostDeserializer implements JsonDeserializer<BlogPost> {
    public static BlogPost DeserializeFrom(JsonObject jsonObject) {
        BlogPost response = new BlogPost();
        if (jsonObject.has("id")) {
            response.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonPrimitive()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                if (titleObject.has("rendered")) {
                    response.title = titleObject.get("rendered").getAsString();
                }
            }
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                if (contentObject.has("rendered")) {
                    response.content = contentObject.get("rendered").getAsString();
                }
            }
        }
        if (jsonObject.has("link")) {
            response.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("slug")) {
            response.slug = jsonObject.get("slug").getAsString();
        }
        if (jsonObject.has("better_featured_image")) {
            JsonElement featuredImageElement = jsonObject.get("better_featured_image");
            if (!featuredImageElement.isJsonPrimitive()) {
                JsonObject featuredImageObject = featuredImageElement.getAsJsonObject();
                if (featuredImageObject.has("source_url")) {
                    response.betterFeatureImage = featuredImageObject.get("source_url").getAsString();
                }
            }
        }
        return response;
    }

    @Override
    public BlogPost deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        return BlogPostDeserializer.DeserializeFrom(jsonObject);
    }
}
