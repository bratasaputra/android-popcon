package com.live.bratasaputra.popcon.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by bratasaputra on 4/30/17.
 */

public class ScheduleDeserializer implements JsonDeserializer<Schedule> {
    public static Schedule DeserializeFrom(JsonObject jsonObject) {
        Schedule schedule = new Schedule();
        if (jsonObject.has("id")) {
            schedule.id = jsonObject.get("id").getAsInt();
        }
        if (jsonObject.has("slug")) {
            schedule.slug = jsonObject.get("slug").getAsString();
        }
        if (jsonObject.has("link")) {
            schedule.link = jsonObject.get("link").getAsString();
        }
        if (jsonObject.has("title")) {
            JsonElement titleElement = jsonObject.get("title");
            if (!titleElement.isJsonPrimitive()) {
                JsonObject titleObject = titleElement.getAsJsonObject();
                if (titleObject.has("rendered")) {
                    schedule.title = titleObject.get("rendered").getAsString();
                }
            }
        }
        if (jsonObject.has("content")) {
            JsonElement contentElement = jsonObject.get("content");
            if (!contentElement.isJsonPrimitive()) {
                JsonObject contentObject = contentElement.getAsJsonObject();
                if (contentObject.has("rendered")) {
                    schedule.content = contentObject.get("rendered").getAsString();
                }
            }
        }
        if (jsonObject.has("time_start_1")) {
            schedule.timeStart1 = jsonObject.get("time_start_1").getAsString();
        }
        if (jsonObject.has("time_end_1")) {
            schedule.timeEnd1 = jsonObject.get("time_end_1").getAsString();
        }
        if (jsonObject.has("time_start_2")) {
            schedule.timeStart2 = jsonObject.get("time_start_2").getAsString();
        }
        if (jsonObject.has("time_end_2")) {
            schedule.timeEnd2 = jsonObject.get("time_end_2").getAsString();
        }
        if (jsonObject.has("time_start_3")) {
            schedule.timeStart3 = jsonObject.get("time_start_3").getAsString();
        }
        if (jsonObject.has("time_end_3")) {
            schedule.timeEnd3 = jsonObject.get("time_end_3").getAsString();
        }
        if (jsonObject.has("company")) {
            JsonElement companyElement = jsonObject.get("company");
            if (companyElement.isJsonArray()) {
                schedule.companyList = new ArrayList<>();
                JsonArray companyArray = companyElement.getAsJsonArray();
                for (JsonElement element :
                        companyArray) {
                    JsonObject companyObject = element.getAsJsonObject();
                    Company company = CompanyDeserializer.DeserializeFrom(companyObject);
                    if (company != null) {
                        schedule.companyList.add(company);
                    }
                }
            }
        }
        if (jsonObject.has("exhibitor")) {
            JsonElement exhibitorElement = jsonObject.get("exhibitor");
            if (exhibitorElement.isJsonArray()) {
                schedule.exhibitorList = new ArrayList<>();
                JsonArray exhibitorArray = exhibitorElement.getAsJsonArray();
                for (JsonElement element :
                        exhibitorArray) {
                    JsonObject exhibitorObject = element.getAsJsonObject();
                    Exhibitor exhibitor = ExhibitorDeserializer.DeserializeFrom(exhibitorObject);
                    if (exhibitor != null) {
                        schedule.exhibitorList.add(exhibitor);
                    }
                }
            }
        }
        if (jsonObject.has("activities")) {
            JsonElement activityElement = jsonObject.get("activities");
            if (activityElement.isJsonArray()) {
                JsonArray activityArray = activityElement.getAsJsonArray();
                schedule.activityList = new ArrayList<>();
                for (JsonElement element :
                        activityArray) {
                    JsonObject activityObject = element.getAsJsonObject();
                    Activities activity = ActivitiesDeserializer.DeserializeFrom(activityObject);
                    if (activity != null) {
                        schedule.activityList.add(activity);
                    }
                }
            }
        }
        if (jsonObject.has("people")) {
            JsonElement speakerElement = jsonObject.get("people");
            if (speakerElement.isJsonArray()) {
                JsonArray speakerArray = speakerElement.getAsJsonArray();
                schedule.speakerList = new ArrayList<>();
                for (JsonElement element :
                        speakerArray) {
                    JsonObject speakerObject = element.getAsJsonObject();
                    Speaker speaker = SpeakerDeserializer.DeserializeFrom(speakerObject);
                    if (speaker != null) {
                        schedule.speakerList.add(speaker);
                    }
                }
            }
        }
        return schedule;
    }

    @Override
    public Schedule deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return ScheduleDeserializer.DeserializeFrom(jsonObject);
    }
}
