package com.live.bratasaputra.popcon.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.ScheduleItem;

import java.util.List;

/**
 * Created by bratasaputra on 8/1/17.
 */

public class ScheduleItemAdapter extends RecyclerView.Adapter<ScheduleItemVH> {
    List<ScheduleItem> scheduleItems;

    public ScheduleItemAdapter(@NonNull List<ScheduleItem> items) {
        this.scheduleItems = items;
    }

    public void setData(@NonNull List<ScheduleItem> items) {
        this.scheduleItems = items;
        notifyDataSetChanged();
    }


    @Override
    public ScheduleItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item_card, parent, false);
        return new ScheduleItemVH(view);
    }

    @Override
    public void onBindViewHolder(ScheduleItemVH holder, int position) {
        holder.bind(scheduleItems.get(position));
    }

    @Override
    public int getItemCount() {
        return scheduleItems.size();
    }
}
