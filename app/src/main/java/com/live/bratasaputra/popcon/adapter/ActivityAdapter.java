package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.Activities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class ActivityAdapter extends RecyclerView.Adapter<ActivityViewHolder> {
    private List<Activities> mActivities = new ArrayList<>();
    private OnItemClickListener mListener = null;
    private Context mContext = null;

    public ActivityAdapter(@NonNull List<Activities> activities, @NonNull Context context) {
        mActivities = activities;
        mContext = context;
    }

    public void addData(List<Activities> activities) {
        mActivities.addAll(activities);
        notifyDataSetChanged();
    }

    public List<Activities> getData() {
        return mActivities;
    }

    public void setData(List<Activities> activities) {
        this.mActivities = activities;
        notifyDataSetChanged();
    }

    @Override
    public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_card_view, parent, false);
        ActivityViewHolder holder = new ActivityViewHolder(view, mContext);
        return holder;
    }

    @Override
    public void onBindViewHolder(ActivityViewHolder holder, int position) {
        holder.bind(mActivities.get(position));
    }

    @Override
    public int getItemCount() {
        return mActivities.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void OnItemClick(Activities item);
    }
}
