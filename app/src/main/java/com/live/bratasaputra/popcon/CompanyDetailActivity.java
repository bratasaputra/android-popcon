package com.live.bratasaputra.popcon;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.bratasaputra.popcon.event.OpenCompanyEvent;
import com.live.bratasaputra.popcon.models.Company;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDetailActivity extends AppCompatActivity {
    protected Company mCompany;

    protected Toolbar mToolbar;
    protected ImageView mImage;
    protected TextView mTitle;
    protected TextView mDesc;
    protected Button mLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImage = (ImageView) findViewById(R.id.company_image);
        mTitle = (TextView) findViewById(R.id.company_title);
        mDesc = (TextView) findViewById(R.id.company_desc);
        mLink = (Button) findViewById(R.id.company_link);

        OpenCompanyEvent event = EventBus.getDefault().removeStickyEvent(OpenCompanyEvent.class);
        if (event == null) {
            finish();
        } else {
            mCompany = event.getItem();

            if (mCompany.featuredMediaUrl == null || mCompany.content == null) {
                PopCon.getDefaultApiClient().GetCompany(mCompany.id).enqueue(new Callback<Company>() {
                    @Override
                    public void onResponse(Call<Company> call, Response<Company> response) {
                        switch (response.code()) {
                            case 200:
                                mCompany = response.body();
                                updateContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<Company> call, Throwable t) {

                    }
                });
            } else {
                updateContent();
            }
        }
    }

    public void updateContent() {
        Glide.with(mImage.getContext()).load(mCompany.featuredMediaUrl).into(mImage);
        mTitle.setText(Html.fromHtml(mCompany.title));
        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mCompany.link));
                startActivity(intent);
            }
        });
        Document document = Jsoup.parse(mCompany.content);
        Element content = document.body().children().first();
        if (content != null) {
            mDesc.setText(content.text());
        } else {
            mDesc.setText(mCompany.content);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
