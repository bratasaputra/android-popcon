package com.live.bratasaputra.popcon;

import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.event.OpenExhibitorEvent;
import com.live.bratasaputra.popcon.models.Exhibitor;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExhibitorDetailActivity extends AppCompatActivity {
    protected Exhibitor mItem;

    protected Toolbar mToolbar;
    protected ImageView mImage;
//    protected TextView mTitle;
    protected TextView mBooth;
    protected TextView mDescription;
    protected Button mLink;
    FirebaseAnalytics analytics;
//    private WebView test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitor_detail);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        OpenExhibitorEvent event = EventBus.getDefault().removeStickyEvent(OpenExhibitorEvent.class);
        if (event != null) {
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, event.getItem().id);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event.getItem().title);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Exhibitor");
            analytics = FirebaseAnalytics.getInstance(this);
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            mItem = event.getItem();
            mImage = (ImageView) findViewById(R.id.exhibitor_image);
//            mTitle = (TextView) findViewById(R.id.exhibitor_title);
            mBooth = (TextView) findViewById(R.id.exhibitor_booth);
            mDescription = (TextView) findViewById(R.id.exhibitor_desc);
//            test = (WebView) findViewById(R.id.webview);
            mLink = (Button) findViewById(R.id.exhibitor_link);
            if (mItem.featuredMediaUrl == null || mItem.content == null) {
                PopCon.getDefaultApiClient().GetExhibitors(mItem.id).enqueue(new Callback<Exhibitor>() {
                    @Override
                    public void onResponse(Call<Exhibitor> call, Response<Exhibitor> response) {
                        switch (response.code()) {
                            case 200:
                                mItem = response.body();
                                updateContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<Exhibitor> call, Throwable t) {

                    }
                });
            } else {
                updateContent();
            }
        } else {
            finish();
        }
    }

    public void updateContent() {
        Document title = Jsoup.parse(mItem.title);
        getSupportActionBar().setTitle(title.text());
        Glide.with(this).load(mItem.featuredMediaUrl).placeholder(R.drawable.placeholder).fitCenter().into(mImage);
//        mTitle.setText(title.text());
        String boothNumber = String.format(Locale.getDefault(), "Booth: %s", mItem.boothNumber);
        if (mItem.boothNumber2 != null) {
            boothNumber = String.format("%s - %s", boothNumber, mItem.boothNumber2);
        }
        mBooth.setText(boothNumber);
        Document document = Jsoup.parse(mItem.content);
        if (document != null) {
            mDescription.setText(document.body().text());
        } else {
            mDescription.setText(mItem.content);
        }
//        test.loadData(mItem.content, "text/html", null);
        if (TextUtils.isEmpty(mItem.link)) {
            mLink.setVisibility(View.GONE);
        }
        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(v.getContext(), Uri.parse(mItem.link));
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.link));
//                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
