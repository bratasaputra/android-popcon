package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.Company;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenCompanyEvent extends OpenItemEvent<Company> {
    public OpenCompanyEvent(Company item) {
        super(item);
        setItem(item);
    }
}
