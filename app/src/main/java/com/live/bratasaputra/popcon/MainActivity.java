package com.live.bratasaputra.popcon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.bratasaputra.popcon.event.ActivityItemClickEvent;
import com.live.bratasaputra.popcon.event.BlogItemClickEvent;
import com.live.bratasaputra.popcon.event.ExhibitorItemClickEvent;
import com.live.bratasaputra.popcon.event.OpenActivityEvent;
import com.live.bratasaputra.popcon.event.OpenBlogEvent;
import com.live.bratasaputra.popcon.event.OpenExhibitorEvent;
import com.live.bratasaputra.popcon.event.OpenSpeakerEvent;
import com.live.bratasaputra.popcon.event.SpeakerItemClickEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private BottomNavigationView mNavigationView;
    private FragmentManager mFragmentManager;

    private int lastSelectedPage = 0;

    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PopCon.init(this);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);

        mNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        disableShiftMode(mNavigationView);
        mNavigationView.setOnNavigationItemSelectedListener(this);
//        mNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
//            @Override
//            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.nav_home:
////                        mFragmentManager.beginTransaction().replace(R.id.content, HomeFragment.Create()).commit();
////                        return true;
//                        Log.d("POPCON", "home");
//                        break;
//                    case R.id.nav_activities:
//                        Util.Log("Activities");
////                        mFragmentManager.beginTransaction().replace(R.id.content, ActivityListFragment.Create()).commit();
////                        return true;
//                        break;
//                    case R.id.nav_exhibitors:
////                        mFragmentManager.beginTransaction().replace(R.id.content, ExhibitorListFragment.Create()).commit();
////                        return true;
//                        Log.d("POPCON", "exh");
//                        break;
//                    case R.id.nav_popstar:
////                        mFragmentManager.beginTransaction().replace(R.id.content, SpeakerListFragment.Create()).commit();
////                        return true;
//                        Log.d("POPCON", "pstar");
//                        break;
//                    case R.id.nav_schedule:
////                        mFragmentManager.beginTransaction().replace(R.id.content, ScheduleListFragment.Create()).commit();
////                        return true;
//                        Log.d("POPCON", "sche");
//                        break;
//                }
//            }
//        });
        mFragmentManager = getSupportFragmentManager();

        mViewPager = (ViewPager) findViewById(R.id.content);
        if (mViewPager != null) {
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            mNavigationView.setSelectedItemId(R.id.nav_home);
                            break;
                        case 1:
                            mNavigationView.setSelectedItemId(R.id.nav_activities);
                            break;
                        case 2:
                            mNavigationView.setSelectedItemId(R.id.nav_exhibitors);
                            break;
                        case 3:
                            mNavigationView.setSelectedItemId(R.id.nav_schedule);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            setupViewPager(mViewPager);
        }

        mNavigationView.setSelectedItemId(R.id.nav_home);

        analytics = FirebaseAnalytics.getInstance(this);
//        mTabLayout = (TabLayout) findViewById(R.id.main_tablayout);
//        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        if (item.getItemId() == lastSelectedPage)
//            return true;
//        lastSelectedPage = item.getItemId();
        switch (item.getItemId()) {
            case R.id.nav_home:
                mViewPager.setCurrentItem(0);
//                mFragmentManager.beginTransaction().replace(R.id.content, HomeFragment.Create()).commit();
                return true;
            case R.id.nav_activities:
                mViewPager.setCurrentItem(1);
//                mFragmentManager.beginTransaction().replace(R.id.content, PromotionFragment.newInstance()).commit();
                return true;
            case R.id.nav_exhibitors:
                mViewPager.setCurrentItem(2);
//                mFragmentManager.beginTransaction().replace(R.id.content, InterestFragment.newInstance()).commit();
                return true;
            case R.id.nav_schedule:
                mViewPager.setCurrentItem(3);
//                mFragmentManager.beginTransaction().replace(R.id.content, ScheduleFragment.newInstance()).commit();
                return true;
        }
        return false;
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(mFragmentManager) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return HomeFragment.Create();
                    case 1:
                        return PromotionFragment.newInstance();
                    case 2:
                        return InterestFragment.newInstance();
                    case 3:
                        return ScheduleFragment.newInstance();
                }
                return null;
            }

            @Override
            public int getCount() {
                return 4;
            }
        };
//        Adapter adapter = new Adapter(getSupportFragmentManager());
//        adapter.addFragment(HomeFragment.Create(), "Home");
//        adapter.addFragment(PromotionFragment.newInstance(), "Promotion");
//        adapter.addFragment(InterestFragment.newInstance(), "Interest");
//        adapter.addFragment(ScheduleFragment.newInstance(), "Schedule");
//        adapter.addFragment(HomeFragment.Create(), "Home");
//        adapter.addFragment(ExhibitorListFragment.Create(), "Exhibitor");
//        adapter.addFragment(ScheduleListFragment.Create(), "Schedule");
//        adapter.addFragment(SpeakerListFragment.Create(), "Popstar");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void OnItemClickEvent(BlogItemClickEvent event) {
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, event.getItem().id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event.getItem().title);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "BlogPost, Home");
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        EventBus.getDefault().postSticky(new OpenBlogEvent(event.getItem()));
//        ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this, event.mView, "blog_image");
//        startActivity(new Intent(this, BlogDetailActivity.class), activityOptions.toBundle());
        startActivity(new Intent(this, BlogDetailActivity.class));
    }

    @Subscribe
    public void OnItemClickEvent(ActivityItemClickEvent event) {
        EventBus.getDefault().postSticky(new OpenActivityEvent(event.getItem()));
        startActivity(new Intent(this, ActivityDetailActivity.class));
    }

    @Subscribe
    public void OnItemClickEvent(ExhibitorItemClickEvent event) {
        EventBus.getDefault().postSticky(new OpenExhibitorEvent(event.getItem()));
        startActivity(new Intent(this, ExhibitorDetailActivity.class));
    }

    @Subscribe
    public void OnItemClickEvent(SpeakerItemClickEvent event) {
        EventBus.getDefault().postSticky(new OpenSpeakerEvent(event.getItem()));
        startActivity(new Intent(this, SpeakerDetailActivity.class));
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }
}
