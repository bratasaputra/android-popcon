package com.live.bratasaputra.popcon.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.bratasaputra.popcon.R;
import com.live.bratasaputra.popcon.models.BlogPost;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bratasaputra on 2/19/17.
 */

public class BlogPostAdapter extends RecyclerView.Adapter<BlogPostViewHolder> {
    private List<BlogPost> mPosts = new ArrayList<>();
    private OnItemClickListener mListener = null;
    private Context mContext;

    public BlogPostAdapter(List<BlogPost> responses) {
        if (responses != null) {
            this.mPosts = responses;
        }
    }

    @Override
    public BlogPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_card_view, parent, false);
        BlogPostViewHolder holder = new BlogPostViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(BlogPostViewHolder holder, int position) {
        holder.bind(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        if (mPosts != null)
            return mPosts.size();
        return 0;
    }

    public List<BlogPost> getData() {
        return this.mPosts;
    }

    public void setData(List<BlogPost> responses) {
        this.mPosts = responses;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void OnItemClick(BlogPost item);
    }
}
