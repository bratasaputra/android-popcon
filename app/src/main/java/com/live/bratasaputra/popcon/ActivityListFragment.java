package com.live.bratasaputra.popcon;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.live.bratasaputra.popcon.adapter.ActivityAdapter;
import com.live.bratasaputra.popcon.models.Activities;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityListFragment extends Fragment {
    protected RecyclerView mRecyclerView = null;
    protected RecyclerView.LayoutManager mLayoutManager = null;
    protected ActivityAdapter mAdapter = null;
    protected ProgressBar mProgressBar = null;

    protected int currentPage;
    protected boolean isDownloading;
    protected int limit;
    protected Realm realm;
    protected List<Activities> tempList;

    protected Callback<List<Activities>> loadCallback = new Callback<List<Activities>>() {
        @Override
        public void onResponse(Call<List<Activities>> call, Response<List<Activities>> response) {
            switch (response.code()) {
                case 200:
                    List<Activities> body = response.body();
                    tempList.addAll(body);
                    if (body != null) {
                        if (body.size() >= limit) {
                            PopCon.getDefaultApiClient().GetActivities(currentPage, limit).enqueue(loadCallback);
                        } else {
                            realm.beginTransaction();
                            realm.delete(Activities.class);
                            realm.copyToRealmOrUpdate(tempList);
                            realm.commitTransaction();
                            reloadData();
                        }
                    }
                    break;
                default:
                    reloadData();
                    break;
            }
        }

        @Override
        public void onFailure(Call<List<Activities>> call, Throwable t) {
            reloadData();
        }
    };

    public ActivityListFragment() {
        // Required empty public constructor
    }

    public static ActivityListFragment Create() {
        ActivityListFragment fragment = new ActivityListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activity_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.blog_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ActivityAdapter(new ArrayList<Activities>(), getContext());
        mRecyclerView.setAdapter(mAdapter);
        mProgressBar = (ProgressBar) view.findViewById(R.id.blogs_load_progress);

        currentPage = 0;
        limit = 100;
        isDownloading = false;
        tempList = new ArrayList<>();
        realm = Realm.getDefaultInstance();

        hideLoading();

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            currentPage = 0;
            limit = 100;
            mAdapter.setData(new ArrayList<Activities>());
            tempList.clear();
            loadActivities();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        reloadData();
        loadActivities();
    }

    private void reloadData() {
        isDownloading = false;
        hideLoading();

        RealmResults<Activities> results = realm.where(Activities.class).findAll();
        mAdapter.setData(results);
    }

    public void loadActivities() {
        if (mAdapter.getItemCount() == 0) {
            showLoading();
        }
        currentPage = currentPage + 1;
        isDownloading = true;

        PopCon.getDefaultApiClient().GetActivities(currentPage, limit).enqueue(loadCallback);
    }

    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }
}
