package com.live.bratasaputra.popcon.event;

import com.live.bratasaputra.popcon.models.BlogPost;

/**
 * Created by bratasaputra on 3/2/17.
 */

public class OpenBlogEvent extends OpenItemEvent<BlogPost> {
    public OpenBlogEvent(BlogPost item) {
        super(item);
        setItem(item);
    }

}
